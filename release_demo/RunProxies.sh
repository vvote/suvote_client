#!/bin/bash
cd ..
openbrowserIn3Seconds(){
echo "Will open browser in 3 seconds"
sleep 3
if which xdg-open > /dev/null
then
  xdg-open http://localhost:8061/demo/demo.html
elif which gnome-open > /dev/null
then
  gnome-open http://localhost:8061/demo/demo.html
fi
}

openbrowserIn3Seconds &
export PEERNAME="EVM"

fifo=$(mktemp -u) &&
  mkfifo "$fifo" &&
  (rm "$fifo" && { java -Dlogback.configurationFile=./release_demo/logback-demo.xml -cp "./release/vecclient.jar:.:./libs/*.jar:./external_libs/*.jar" uk.ac.surrey.cs.tvs.web.ProxyServer 8060 "./release_demo/wwwroot/" ./release_demo/TestEVMOne/client.conf <&3 3<&-; } &) 3<> "$fifo"
export PEERNAME="VPS"
java -Dlogback.configurationFile=./release_demo/logback-demo.xml -cp "./release/vecclient.jar:.:./libs/*.jar:./external_libs/*.jar" uk.ac.surrey.cs.tvs.web.ProxyServer 8061 "./release_demo/wwwroot/" ./release_demo/TestDeviceOne/client.conf
trap "kill 0" SIGINT SIGTERM EXIT

