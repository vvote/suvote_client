/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.client;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.utils.DefaultTimeoutFileFilter;
import uk.ac.surrey.cs.tvs.utils.FileBackedTimeout;
import uk.ac.surrey.cs.tvs.utils.FileBackedTimeoutManager;
import uk.ac.surrey.cs.tvs.utils.exceptions.MaxTimeoutExceeded;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * DeleteBallotWorker is the inner Timeout class that is run on expiration of a timeout and deletes the specified ballot. This is an
 * extension of the FileBackedTimeout abstract class and should be used with a FileBackedTimeoutManager. This allows it to create
 * timeout files on the hard disk and still enforce the timeout even if the system is shutdown (controlled or unexpectedly) before
 * the timeout has fired.
 * 
 * Care has been taken when implementing this to comply with the necessary restrictions and as such any changes/subclasses should
 * take equal care. Of particular note is that the performRecovery method should not rely on instance variable unless it can
 * guarantee they have been set in advanced. This is because during a recovery a default constructed DeleteBallotWorker is used to
 * rebuild the timeout list.
 * 
 * It is also important that setBackUpDir is called before any operations are performed on the hard disk based backups. This is left
 * as an option because in some settings the backUpDir might be hard coded or place in a temp directory. It is also problematic in
 * terms of compatibility with non-file backed timeouts to place it in the constructor.
 * 
 * @author Chris Culnane
 * 
 */
public class DeleteBallotWorker extends FileBackedTimeout {

  /**
   * Logger
   */
  private static final Logger logger        = LoggerFactory.getLogger(DeleteBallotWorker.class);

  /**
   * Field in the JSON stored on the disk that contains the path to the ballotFile that needs deleting
   */
  private static final String FILE_FIELD    = "ballotFile";

  /**
   * Field in the JSON that holds the timeout point for this operation (this is pre-calculated as the currentTime+timeout at first
   * instantiation
   */
  private static final String TIMEOUT_FIELD = "timeout";

  /**
   * String variable to hold file path for the ballot file
   */
  private String              ballotFile;

  /**
   * File that points to the location of the backupDir - this must be set via setBackUpDir
   */
  private File                backupDir;

  /**
   * Default constructor used for create an object to call perform recovery on
   */
  public DeleteBallotWorker() {
    super();
  }

  /**
   * Creates a new DeleteBallotWorker to delete the specified file once the timeout is expired
   * 
   * @param ballotFile
   *          String path to ballot file to delete
   */
  public DeleteBallotWorker(String ballotFile) {
    super();
    this.ballotFile = ballotFile;
  }

  /**
   * Runs the required task.
   * 
   * @see java.lang.Runnable#run()
   */
  @Override
  public void run() {
    File ballot = new File(this.ballotFile);
    performCheck(ballot);

  }

  /**
   * Internal method for actually doing the check and deletion. This is put in a separate method to allow reuse when performing a
   * recovered timeout from disk.
   * 
   * @param ballot
   *          File that points to the ballot being checked
   */
  private void performCheck(File ballot) {
    if (ballot.exists()) {
      boolean result = ballot.delete();
      logger.info("Deleting Ballot {}. Success:{}", ballot, result);
    }
    else {
      logger.warn("Could not delete Ballot {}, it does not exist", ballot);
    }
    this.removeBackup(ballot, this.backupDir);
  }

  /**
   * Creates a JSONObject containing the filepath and timeout and saves it to the backupDir using the fileName of the related ballot
   * file as a unique file name. Uses the default extension to allow easy filtering.
   */
  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.surrey.cs.tvs.utils.FileBackedTimeout#createFileBackup(int)
   */
  @Override
  public void createFileBackup(int timeout) throws IOException {
    try {
      // Create the JSON Object and set the field values
      JSONObject timeoutTask = new JSONObject();
      timeoutTask.put(FILE_FIELD, this.ballotFile);
      timeoutTask.put(TIMEOUT_FIELD, (System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(timeout)));
      File ballotFileFull = new File(ballotFile);

      // having going the full path to the ballot file, get the filename and build the timeout file
      IOUtils.writeJSONToFile(timeoutTask, new File(backupDir, ballotFileFull.getName()
          + DefaultTimeoutFileFilter.DEFAULT_TIMEOUT_FILE_EXT).getAbsolutePath());
    }
    catch (JSONException e) {
      throw new IOException(e);
    }
    catch (JSONIOException e) {
      throw new IOException(e);
    }

  }

  /**
   * Performs a recovery on a disk based timeout following a shutdown whilst timeouts still existed. This will load each timeout, if
   * it has expired it will call performCheck and delete the ballot file if it still exists. Otherwise it will calculate the
   * appropriate timeout from now and create a new in-memory timeout.
   * 
   * @param file
   *          File that points to the hard disk based ballot file created during createFileBackup
   * @param timeoutManager
   *          FileBackedTimeoutManager the timeout manager to create new timeouts in and to get the BackupDir from
   * @throws IOException
   * @throws MaxTimeoutExceeded
   */
  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.surrey.cs.tvs.utils.FileBackedTimeout#performRecovery(java.io.File)
   */
  @Override
  public void performRecovery(File file, FileBackedTimeoutManager timeoutManager) throws IOException, MaxTimeoutExceeded {
    try {
      // Read the JSON timeout task
      JSONObject timeoutTask = IOUtils.readJSONObjectFromFile(file.getAbsolutePath());
      long timeoutValue = timeoutTask.getLong(TIMEOUT_FIELD);
      // Check if this should have already timed out and if it had do the relevant processing
      if (timeoutValue <= System.currentTimeMillis()) {
        // Gets the ballot file, sets the backUpDir in this instance and calls performCheck (perform check will remove the file
        // associated with this timeout when it is done)
        File ballot = new File(timeoutTask.getString(FILE_FIELD));
        this.backupDir = timeoutManager.getBackupDir();
        this.performCheck(ballot);
      }
      else {
        //The timeout is still in the future, create a new in-memory timeout
        DeleteBallotWorker replacementTimeout = new DeleteBallotWorker(timeoutTask.getString(FILE_FIELD));
        replacementTimeout.setBackUpDir(timeoutManager.getBackupDir());
        // Note: we do not call the addTimeout with the supertype, we pass the interface - this stops a second backup file being
        // generated
        timeoutManager.addTimeout((Runnable) replacementTimeout,
            (int) TimeUnit.MILLISECONDS.toSeconds(timeoutValue - System.currentTimeMillis()));
      }
    }
    catch (JSONIOException e) {
      throw new IOException(e);
    }
    catch (JSONException e) {
      throw new IOException(e);
    }

  }

  /**
   * Deletes the hard disk based timeout file. 
   * @param file File pointing to the ballot file associated with this timeout - used to get filename
   * @param backupDir File backupDir where the timeout files are stored
   */
  public void removeBackup(File file, File backupDir) {
    File toDelete = new File(backupDir, file.getName() + DefaultTimeoutFileFilter.DEFAULT_TIMEOUT_FILE_EXT);
    boolean result = toDelete.delete();
    logger.info("Removed backup file {} successful:{}", toDelete, result);
  }

  /**
   * Sets the backupDir associated with this DeleteBallotWorker - it is essential this is called before any create or recover methods are called.
   * @param backupDir File that points to the location to store hard disk timeouts
   */
  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.surrey.cs.tvs.utils.FileBackedTimeout#setBackUpDir(java.io.File)
   */
  @Override
  public void setBackUpDir(File backupDir) {
    this.backupDir = backupDir;

  }
}
