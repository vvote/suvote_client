/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.web;

import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.ballotgen.exceptions.BallotFileNotFoundException;
import uk.ac.surrey.cs.tvs.ballotgen.exceptions.ProxyMessageProcessingException;
import uk.ac.surrey.cs.tvs.client.Client;
import uk.ac.surrey.cs.tvs.comms.MBB;
import uk.ac.surrey.cs.tvs.comms.MBBResponse;
import uk.ac.surrey.cs.tvs.comms.SimpleResponseChecker;
import uk.ac.surrey.cs.tvs.comms.exceptions.ConsensusException;
import uk.ac.surrey.cs.tvs.comms.exceptions.MBBCommunicationException;
import uk.ac.surrey.cs.tvs.comms.http.NanoServlet;
import uk.ac.surrey.cs.tvs.comms.http.VVoteWebServer;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants.UIMessage;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles.ClientConfig;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageTypes;
import uk.ac.surrey.cs.tvs.fields.messages.SystemConstants;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.io.ConfigFile;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.JSONUtils;
import uk.ac.surrey.cs.tvs.utils.io.MBBConfig;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;
import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.Method;
import fi.iki.elonen.NanoHTTPD.Response;

/**
 * Servlet used to handle audit and cancellation.
 * 
 * @author Chris Culnane
 * 
 */
public class MBBPODProxyServlet implements NanoServlet {

  /**
   * Configuration file for the client.
   */
  private ConfigFile          clientConf;

  /**
   * Holds a reference of the schema for the JSON message this proxy accepts
   */
  private String              schema;

  /**
   * Path to the schema file
   */
  private static String       SCHEMA_PATH = "/sdcard/vvoteclient/schemas/mbbpodproxyschema.json";

  /**
   * Configuration file for the district information.
   */
  private JSONObject          districtConf;

  /**
   * Logger
   */
  private static final Logger logger      = LoggerFactory.getLogger(MBBPODProxyServlet.class);

  /**
   * Client object to get values from
   */
  private Client              client;
  
  /**
   * TVSKeyStore that contains the certificates to verify the message from the MBB
   */
  private TVSKeyStore         clientKeyStore;

  /**
   * Constructor requiring the configuration information for the election and client.
   * 
   * @param client
   *          Client that this MBBPODProxy relates to
   * 
   * @param districtConfFile
   *          String path to district config file
   * 
   * @throws JSONIOException
   * @throws CryptoIOException
   */
  public MBBPODProxyServlet(Client client, String districtConfFile) throws JSONIOException, CryptoIOException {
    super();

    logger.info("Loading MBBPODProxyServlet");
    this.client = client;
    this.clientConf = this.client.getClientConf();
    this.districtConf = IOUtils.readJSONObjectFromFile(districtConfFile);
    this.schema = JSONUtils.loadSchema(MBBPODProxyServlet.SCHEMA_PATH);
    this.clientKeyStore = CryptoUtils.loadTVSKeyStore(this.client.getClientConf().getStringParameter(
        ClientConfig.SYSTEM_CERTIFICATE_STORE));
    logger.info("Finished loading MBBPODProxyServlet");
  }

  /**
   * Prepares an audit to be sent to the MBB.
   * 
   * @param submitSig
   *          The submission signature.
   * @param messageSent
   *          The message to be sent.
   * @return JSONArray of ballot reductions
   * @throws JSONException
   * @throws TVSSignatureException
   * @throws JSONIOException
   * @throws ProxyMessageProcessingException
   * @throws BallotFileNotFoundException
   */
  private JSONArray prepareAuditMessage(TVSSignature submitSig, JSONObject messageSent) throws JSONException,
      TVSSignatureException, JSONIOException, ProxyMessageProcessingException, BallotFileNotFoundException {

    String serialNo = messageSent.getString(ClientConstants.UIMessage.SERIAL_NO);
    logger.info("Preparing audit message for {}", serialNo);
    submitSig.update(messageSent.getString(ClientConstants.UIMessage.TYPE));
    String ballotOutput = this.clientConf.getStringParameter(ConfigFiles.ClientConfig.BALLOT_OUTPUT_FOLDER);

    // Get the ballot files
    File ballotFile = new File(ballotOutput + File.separator + IOUtils.getFileNameFromSerialNo(serialNo)
        + MessageFields.Ballot.BALLOT_FILE_EXTENSION);

    if (ballotFile.exists()) {
      // Check the ballot exists and then open it
      JSONObject ballot = IOUtils.readJSONObjectFromFile(ballotFile.getAbsolutePath());
      logger.info("Got ballot");
      // Convert the permutation array into a string
      JSONArray perms = ballot.getJSONArray(MessageFields.Ballot.PERMUTATIONS);
      StringBuffer commitPermutation = new StringBuffer();

      for (int i = 0; i < perms.length(); i++) {
        commitPermutation.append(perms.getJSONArray(i).join(MessageFields.PREFERENCE_SEPARATOR));
        commitPermutation.append(MessageFields.RACE_SEPARATOR);
      }

      // Sign the permutation and add the data to the message to send to the MBB
      submitSig.update(commitPermutation.toString());
      submitSig.update(ballot.getString(MessageFields.Ballot.COMMIT_WITNESS));
      messageSent.put(MessageFields.AuditMessage.COMMIT_WITNESS, ballot.getString(MessageFields.Ballot.COMMIT_WITNESS));
      messageSent.put(MessageFields.AuditMessage.PERMUTATION, commitPermutation.toString());

      // Do ballot reduction
      JSONObject districtData = this.districtConf.getJSONObject(messageSent.getString(ClientConstants.UIPODMessage.DISTRICT));
      PODProxyServlet.doBallotReduction(ballot, districtData);

      return ballot.getJSONArray(MessageFields.Ballot.PERMUTATIONS);
    }
    else {
      logger.warn("Ballot does not exist - possibly expired timeout");
      throw new BallotFileNotFoundException("Couldn't find ballot with serial number:" + serialNo);
    }
  }

  /**
   * Prepares a cancel to be sent to the MBB.
   * 
   * @param submitSig
   *          The submission signature.
   * @param messageSent
   *          The message to be sent.
   * @throws JSONException
   * @throws TVSSignatureException
   * @throws JSONIOException
   * @throws ProxyMessageProcessingException
   */
  private void prepareCancelMessage(TVSSignature submitSig, JSONObject messageSent) throws JSONException, TVSSignatureException,
      JSONIOException, ProxyMessageProcessingException {
    // A cancel message signature only signs the type "Cancel", so there is no additional preprocessing
    submitSig.update(messageSent.getString(UIMessage.TYPE));
  }

  /**
   * Processes a servlet message. This proxy only adds/updates data in the original message. It aims to forward the message on with
   * as little change as possible. Hence the cancel message only requires a signature field to be created and nothing else.
   * 
   * @param message
   *          The message to process.
   * @return The message that has been sent to tbe MBB in response to the servlet request.
   * @throws JSONException
   * @throws IOException
   * @throws KeyStoreException
   * @throws NoSuchAlgorithmException
   * @throws CertificateException
   * @throws UnrecoverableKeyException
   * @throws InvalidKeyException
   * @throws SignatureException
   * @throws CryptoIOException
   * @throws TVSSignatureException
   * @throws JSONIOException
   * @throws MBBCommunicationException
   * @throws ProxyMessageProcessingException
   * @throws ConsensusException
   */
  private JSONObject processMessage(String message) throws JSONException, IOException, KeyStoreException, NoSuchAlgorithmException,
      CertificateException, UnrecoverableKeyException, InvalidKeyException, SignatureException, CryptoIOException,
      TVSSignatureException, JSONIOException, MBBCommunicationException, ProxyMessageProcessingException, ConsensusException {
    JSONObject messageSent = new JSONObject(message);
    try {

      TVSKeyStore tvsKeyStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
      tvsKeyStore.load(this.clientConf.getStringParameter(ClientConfig.SIGNING_KEY_STORE), "".toCharArray());

      TVSSignature submitSig = new TVSSignature(SignatureType.BLS, tvsKeyStore.getBLSPrivateKey(ClientConfig.SIGNING_KEY_ALIAS));

      submitSig.update(messageSent.getString(UIMessage.SERIAL_NO));
      JSONArray ballotReductions = null;
      String type = messageSent.getString(UIMessage.TYPE);

      if (type.equalsIgnoreCase(MessageTypes.AUDIT)) {
        ballotReductions = this.prepareAuditMessage(submitSig, messageSent);
      }
      else if (type.equalsIgnoreCase(MessageTypes.CANCEL)) {
        this.prepareCancelMessage(submitSig, messageSent);
      }

      messageSent.put(MessageFields.JSONWBBMessage.SENDER_ID, this.clientConf.getStringParameter(ClientConfig.ID));
      messageSent.put(MessageFields.JSONWBBMessage.SENDER_SIG, submitSig.signAndEncode(EncodingType.BASE64));
      MBBConfig mbbConf = new MBBConfig(this.clientConf.getStringParameter(ClientConfig.MBB_CONF_FILE));

      StringBuffer dataToCheck = new StringBuffer();
      dataToCheck.append(type);
      dataToCheck.append(messageSent.getString(MessageFields.JSONWBBMessage.ID));
      
      if (type.equalsIgnoreCase(MessageTypes.AUDIT)) {
        StringBuffer reducedPerms = new StringBuffer();

        for (int raceCount = 0; raceCount < ballotReductions.length(); raceCount++) {
          reducedPerms.append(ballotReductions.getJSONArray(raceCount).join(MessageFields.PREFERENCE_SEPARATOR));
          reducedPerms.append(MessageFields.RACE_SEPARATOR);
        }

        dataToCheck.append(reducedPerms.toString());
      }

      
      MBBResponse mbbResponse = MBB.sendMessageWaitForThreshold(mbbConf, messageSent.toString(), this.client.getSocketFactory(), this.client
          .getCipherSuite(),
          new SimpleResponseChecker(this.clientKeyStore, SystemConstants.SIGNING_KEY_SK2_SUFFIX, dataToCheck.toString()),
          this.clientConf.getIntParameter(ClientConfig.THRESHOLD));
      
      TVSSignature tvsSig = new TVSSignature(SignatureType.BLS, this.clientKeyStore);
      tvsSig.update(dataToCheck.toString());

      JSONObject response = tvsSig.verifyAndCombine(mbbResponse.getResponseArray(), SystemConstants.SIGNING_KEY_SK2_SUFFIX,
          this.clientConf.getIntParameter(ClientConfig.THRESHOLD), mbbConf.getPeers().size(),!mbbResponse.isThresholdChecked(),true);
      if (response.has(TVSSignature.COMBINED)) {
        logger.info("Signature on response verified");
        messageSent.put(ClientConstants.UIResponse.WBB_SIGNATURES, response.getString(TVSSignature.COMBINED));
        if (ballotReductions != null) {
          // Prepare response data for client
          JSONArray races = new JSONArray();
          JSONObject laRace = new JSONObject();
          JSONObject lcATLRace = new JSONObject();
          JSONObject lcBTLRace = new JSONObject();

          laRace.put(ClientConstants.UIAUDITResponse.RACE_ID, ClientConstants.UIPODResponse.DISTRICT_RACE);
          laRace.put(ClientConstants.UIAUDITResponse.RACE_PERMUTATION, ballotReductions.getJSONArray(0));

          lcATLRace.put(ClientConstants.UIAUDITResponse.RACE_ID, ClientConstants.UIPODResponse.REGION_RACE_ATL);
          lcATLRace.put(ClientConstants.UIAUDITResponse.RACE_PERMUTATION, ballotReductions.getJSONArray(1));
          lcBTLRace.put(ClientConstants.UIAUDITResponse.RACE_ID, ClientConstants.UIPODResponse.REGION_RACE_BTL);
          lcBTLRace.put(ClientConstants.UIAUDITResponse.RACE_PERMUTATION, ballotReductions.getJSONArray(2));

          races.put(laRace);
          races.put(lcATLRace);
          races.put(lcBTLRace);
          messageSent.remove(MessageFields.AuditMessage.COMMIT_WITNESS);
          messageSent.remove(MessageFields.AuditMessage.PERMUTATION);

          messageSent.put(ClientConstants.UIPODResponse.RACES, races);
        }
      }
      else {
        messageSent = ProxyServer.constructErrorMessage(mbbResponse.getResponseArray(), "Exception when sending message to MBB");
        logger.error("Exception in sending message");
      }
    }
    catch (BallotFileNotFoundException e) {
      messageSent = ProxyServer.constructErrorMessage("Ballot file not found");
      logger.error("Exception in sending message");
    }
    return messageSent;

  }

  /**
   * Runs the servlet.
   * 
   * @param uri
   *          String
   * @param method
   *          Method
   * @param header
   *          Map<String,String>
   * @param params
   *          Map<String,String>
   * @param files
   *          Map<String,String>
   * @param homeDir
   *          File
   * @return Response
   * 
   * @see uk.ac.surrey.cs.tvs.comms.http.NanoServlet#runServlet(java.lang.String, fi.iki.elonen.NanoHTTPD.Method, java.util.Map,
   *      java.util.Map, java.util.Map, java.io.File)
   */
  @Override
  public Response runServlet(String uri, Method method, Map<String, String> header, Map<String, String> params,
      Map<String, String> files, File homeDir) {
    if (params.containsKey(ClientConstants.REQUEST_MESSAGE_PARAM)) {
      try {
        logger.info("received: {}", params.get(ClientConstants.REQUEST_MESSAGE_PARAM));
        if (JSONUtils.validateSchema(this.schema, params.get(ClientConstants.REQUEST_MESSAGE_PARAM))) {
          return new Response(Response.Status.OK, VVoteWebServer.MIME_JSON, this.processMessage(
              params.get(ClientConstants.REQUEST_MESSAGE_PARAM)).toString());
        }
        else {
          return new Response(Response.Status.BAD_REQUEST, NanoHTTPD.MIME_PLAINTEXT, "Message validation failed");
        }
      }
      catch (UnrecoverableKeyException e) {
        logger.error("Exception whilst processing message", e);
        return new Response(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
      catch (InvalidKeyException e) {
        logger.error("Exception whilst processing message", e);
        return new Response(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
      catch (KeyStoreException e) {
        logger.error("Exception whilst processing message", e);
        return new Response(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
      catch (NoSuchAlgorithmException e) {
        logger.error("Exception whilst processing message", e);
        return new Response(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
      catch (CertificateException e) {
        logger.error("Exception whilst processing message", e);
        return new Response(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
      catch (SignatureException e) {
        logger.error("Exception whilst processing message", e);
        return new Response(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
      catch (JSONException e) {
        logger.error("Exception whilst processing message", e);
        return new Response(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
      catch (IOException e) {
        logger.error("Exception whilst processing message", e);
        return new Response(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
      catch (CryptoIOException e) {
        logger.error("Exception whilst processing message", e);
        return new Response(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
      catch (TVSSignatureException e) {
        logger.error("Exception whilst processing message", e);
        return new Response(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
      catch (JSONIOException e) {
        logger.error("Exception whilst processing message", e);
        return new Response(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
      catch (MBBCommunicationException e) {
        logger.error("Exception whilst processing message", e);
        return new Response(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
      catch (ProxyMessageProcessingException e) {
        logger.error("Exception whilst processing message", e);
        return new Response(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
      catch (ConsensusException e) {
        logger.error("Exception whilst processing message", e);
        return new Response(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
    }
    else {
      return new Response(Response.Status.BAD_REQUEST, NanoHTTPD.MIME_PLAINTEXT, "Missing msg parameter - no message to process");
    }
  }
}
