/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.web;

import java.io.File;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.ballotgen.exceptions.ProxyException;
import uk.ac.surrey.cs.tvs.client.Client;
import uk.ac.surrey.cs.tvs.client.DeleteBallotWorker;
import uk.ac.surrey.cs.tvs.comms.MBB;
import uk.ac.surrey.cs.tvs.comms.MBBResponse;
import uk.ac.surrey.cs.tvs.comms.SimpleResponseChecker;
import uk.ac.surrey.cs.tvs.comms.exceptions.MBBCommunicationException;
import uk.ac.surrey.cs.tvs.comms.http.NanoServlet;
import uk.ac.surrey.cs.tvs.comms.http.thirdparty.VVoteNanoHTTPD;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants.UIResponse;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles.BallotGenConfig;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles.ClientConfig;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.fields.messages.SystemConstants;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.io.ConfigFile;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.JSONUtils;
import uk.ac.surrey.cs.tvs.utils.io.MBBConfig;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;
import fi.iki.elonen.NanoHTTPD.Method;
import fi.iki.elonen.NanoHTTPD.Response;

/**
 * Servlet used to handle ballot reduction.
 * 
 * @author Chris Culnane
 * 
 * @version $Revision: 1.0 $
 */
public class PODProxyServlet implements NanoServlet {

  /**
   * Configuration file for the election information.
   */
  private ConfigFile          conf;

  /**
   * ConfigFile containing the client config
   */
  private ConfigFile          clientConf;

  /**
   * Client object that this servlet interacts with
   */
  private Client              client;

  /**
   * TVSKeyStore for holding the client keys
   */
  private TVSKeyStore         clientKeyStore;

  /**
   * Configuration file for the district information.
   */
  private JSONObject          districtConf;

  /**
   * Logger
   */
  private static final Logger logger      = LoggerFactory.getLogger(PODProxyServlet.class);

  /**
   * String to hold the schema in
   */
  private String              schema;

  /**
   * Path to the schema that covers messages received by this proxy
   */
  private static String       SCHEMA_PATH = "/sdcard/vvoteclient/schemas/podproxyschema.json";

  /**
   * Creates a new PODProxyServlet that references the configFile and districtConfig as specified. This will handle Print on Demand
   * requests from the client HTML.
   * 
   * 
   * @param districtConfFile
   *          path to district configuration file - number of candidates in each race
   * @param client
   *          Client
   * @throws JSONIOException
   * @throws CryptoIOException
   */
  public PODProxyServlet(Client client, String districtConfFile) throws JSONIOException, CryptoIOException {
    super();

    logger.info("Loading PODProxyServlet");
    this.client = client;
    this.clientConf = this.client.getClientConf();

    this.conf = new ConfigFile(this.clientConf.getStringParameter(ClientConfig.BALLOT_GEN_CONF));
    this.districtConf = IOUtils.readJSONObjectFromFile(districtConfFile);
    this.schema = JSONUtils.loadSchema(PODProxyServlet.SCHEMA_PATH);
    this.clientKeyStore = CryptoUtils.loadTVSKeyStore(this.client.getClientConf().getStringParameter(
        ClientConfig.SYSTEM_CERTIFICATE_STORE));
    logger.info("Finished loading PODProxyServlet");
  }

  /**
   * Process and incoming message string, which should be in JSON format and with the relevant fields.
   * 
   * @param message
   *          JSON String of a message
   * 
   * 
   * @return JSONObject response
   * @throws ProxyException
   */
  private JSONObject processMessage(String message) throws ProxyException {
    try {
      // We know the type is pod because the schema restricts it

      // Load the message object
      JSONObject messageSent = new JSONObject(message);

      // Get the relevant district
      JSONObject districtData = this.districtConf.getJSONObject(messageSent.getString(ClientConstants.UIPODMessage.DISTRICT));

      // Open the ballot list and ballotDB files
      JSONArray ballots = IOUtils.readJSONArrayFromFile(this.conf.getStringParameter(BallotGenConfig.BALLOT_LIST));
      JSONObject ballotDB = IOUtils.readJSONObjectFromFile(this.conf.getStringParameter(BallotGenConfig.BALLOT_DB));

      // Find the next ballot
      String nextBallot = ballots.getString(ballotDB.getInt(ClientConstants.BallotDB.NEXT_BALLOT));

      // increment the BallotDB and save the changes
      ballotDB.increment(ClientConstants.BallotDB.NEXT_BALLOT);
      
      IOUtils.writeJSONToFile(ballotDB, this.conf.getStringParameter(BallotGenConfig.BALLOT_DB));

      // Open the ballot
      JSONArray ballotReductions = new JSONArray();
      File ballotFile = new File(nextBallot);
      if (ballotFile.exists()) {
        JSONObject ballot = IOUtils.readJSONObjectFromFile(ballotFile.getAbsolutePath());
        // Perform ballot reduction
        ArrayList<ArrayList<RemovedCipher>> reductions = PODProxyServlet.doBallotReduction(ballot, districtData);

        // Ballot reductions require providing the combined randomness
        JSONArray randomness = ballot.getJSONArray(MessageFields.Ballot.COMBINED_RANDOMNESS);
        for (int raceCount = 0; raceCount < reductions.size(); raceCount++) {
          JSONArray raceReductRandomness = new JSONArray();
          ArrayList<RemovedCipher> raceReductions = reductions.get(raceCount);

          for (int indexCount = 0; indexCount < raceReductions.size(); indexCount++) {
            JSONObject reduction = new JSONObject();
            RemovedCipher removedCiph = raceReductions.get(indexCount);
            String reductionRand = randomness.getJSONArray(raceCount).getString(removedCiph.getCandidateIndex());
            reduction.put(MessageFields.PODMessage.BALLOT_REDUCTIONS_PERMUTATION_INDEX, removedCiph.getIndex());
            reduction.put(MessageFields.PODMessage.BALLOT_REDUCTIONS_RANDOMNESS, reductionRand);
            reduction.put(MessageFields.PODMessage.BALLOT_REDUCTIONS_CANDIDATE_INDEX, removedCiph.getCandidateIndex());
            raceReductRandomness.put(reduction);
          }

          ballotReductions.put(raceReductRandomness);
        }
        // Add the data to the messageSent - this will be the message that is then sent onto the MBB peers
        messageSent.put(MessageFields.PODMessage.BALLOT_REDUCTIONS, ballotReductions);
        messageSent.put(JSONWBBMessage.ID, ballot.getString(MessageFields.Ballot.SERIAL_NO));
        messageSent.put(JSONWBBMessage.TYPE, messageSent.getString(ClientConstants.UIMessage.TYPE));

        // Prepare to sign the contents of the message

        TVSKeyStore tvsKeyStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
        tvsKeyStore.load(this.clientConf.getStringParameter(ClientConfig.SIGNING_KEY_STORE), "".toCharArray());

        TVSSignature submitSig = new TVSSignature(SignatureType.BLS, tvsKeyStore.getBLSPrivateKey(ClientConfig.SIGNING_KEY_ALIAS));
        submitSig.update(ballot.getString(MessageFields.Ballot.SERIAL_NO));
        submitSig.update(messageSent.getString(ClientConstants.UIPODMessage.DISTRICT));

        // Add the signature to the message
        messageSent.put(JSONWBBMessage.SENDER_ID, this.clientConf.getStringParameter(ClientConfig.ID));
        messageSent.put(JSONWBBMessage.SENDER_SIG, submitSig.signAndEncode(EncodingType.BASE64));

        // Get a reference to the MBB and send the message, waiting for the returned signatures
        MBBConfig mbbConf = new MBBConfig(this.clientConf.getStringParameter(ClientConfig.MBB_CONF_FILE));

        StringBuffer dataToCheck = new StringBuffer();
        dataToCheck.append(messageSent.getString(JSONWBBMessage.ID));
        dataToCheck.append(messageSent.getString(JSONWBBMessage.DISTRICT));
        MBBResponse mbbResponse = MBB.sendMessageWaitForThreshold(mbbConf, messageSent.toString(), this.client.getSocketFactory(), this.client
            .getCipherSuite(),
            new SimpleResponseChecker(this.clientKeyStore, SystemConstants.SIGNING_KEY_SK2_SUFFIX, dataToCheck.toString()),
            this.clientConf.getIntParameter(ClientConfig.THRESHOLD));
        JSONObject response;
        TVSSignature tvsSig = new TVSSignature(SignatureType.BLS, this.clientKeyStore);
        tvsSig.update(messageSent.getString(JSONWBBMessage.ID));
        tvsSig.update(messageSent.getString(JSONWBBMessage.DISTRICT));
        response = tvsSig.verifyAndCombine(mbbResponse.getResponseArray(), SystemConstants.SIGNING_KEY_SK2_SUFFIX,
            this.clientConf.getIntParameter(ClientConfig.THRESHOLD), mbbConf.getPeers().size(),!mbbResponse.isThresholdChecked(),true);
        
        this.client.getTimeoutManager().addDefaultTimeout(new DeleteBallotWorker(nextBallot));

        logger.info("Received response, will check signature");

        
        if (response.has(TVSSignature.COMBINED)) {
          logger.info("Signature on response verified");
          // Prepare response data for client
          JSONArray races = new JSONArray();
          JSONObject laRace = new JSONObject();
          JSONObject lcATLRace = new JSONObject();
          JSONObject lcBTLRace = new JSONObject();

          laRace.put(ClientConstants.UIPODResponse.RACE_ID, ClientConstants.UIPODResponse.DISTRICT_RACE);
          laRace.put(ClientConstants.UIPODResponse.RACE_PERMUTATION, ballot.getJSONArray(MessageFields.Ballot.PERMUTATIONS)
              .getJSONArray(0));

          lcATLRace.put(ClientConstants.UIPODResponse.RACE_ID, ClientConstants.UIPODResponse.REGION_RACE_ATL);
          lcATLRace.put(ClientConstants.UIPODResponse.RACE_PERMUTATION, ballot.getJSONArray(MessageFields.Ballot.PERMUTATIONS)
              .getJSONArray(1));
          lcBTLRace.put(ClientConstants.UIPODResponse.RACE_ID, ClientConstants.UIPODResponse.REGION_RACE_BTL);
          lcBTLRace.put(ClientConstants.UIPODResponse.RACE_PERMUTATION, ballot.getJSONArray(MessageFields.Ballot.PERMUTATIONS)
              .getJSONArray(2));

          races.put(laRace);
          races.put(lcATLRace);
          races.put(lcBTLRace);

          messageSent.put(ClientConstants.UIPODResponse.RACES, races);
          messageSent.remove(MessageFields.PODMessage.BALLOT_REDUCTIONS);
          messageSent.remove(JSONWBBMessage.SENDER_ID);
          messageSent.remove(JSONWBBMessage.SENDER_SIG);
          messageSent.remove(JSONWBBMessage.TYPE);
          messageSent.put(UIResponse.WBB_SIGNATURES, response.getString(TVSSignature.COMBINED));

        }
        else {
          messageSent = ProxyServer.constructErrorMessage(mbbResponse.getResponseArray(), "Signature in MBB response did not validate");
          logger.error("Signature in MBB response did not validate");
        }

        // Return the response
        return messageSent;
      }
      else {
        throw new ProxyException("Ballot does not exist");
      }
    }
    catch (JSONException e) {
      throw new ProxyException("Exception whilst processing message", e);
    }
    catch (JSONIOException e) {
      throw new ProxyException("Exception whilst processing message", e);
    }

    catch (TVSSignatureException e) {
      throw new ProxyException("Exception whilst processing message", e);
    }
    catch (MBBCommunicationException e) {
      throw new ProxyException("Exception whilst processing message", e);
    }

    catch (KeyStoreException e) {
      throw new ProxyException("Exception whilst processing message", e);
    }
  }

  /**
   * Runs the servlet.
   * 
   * 
   * @param uri
   *          String
   * @param method
   *          Method
   * @param header
   *          Map<String,String>
   * @param params
   *          Map<String,String>
   * @param files
   *          Map<String,String>
   * @param homeDir
   *          File
   * @return Response
   * 
   * @see uk.ac.surrey.cs.tvs.comms.http.NanoServlet#runServlet(java.lang.String, fi.iki.elonen.NanoHTTPD.Method, java.util.Map,
   *      java.util.Map, java.util.Map, java.io.File)
   */
  @Override
  public Response runServlet(String uri, Method method, Map<String, String> header, Map<String, String> params,
      Map<String, String> files, File homeDir) {
    if (params.containsKey(ClientConstants.REQUEST_MESSAGE_PARAM)) {
      try {
        if (JSONUtils.validateSchema(this.schema, params.get(ClientConstants.REQUEST_MESSAGE_PARAM))) {
          return new Response(Response.Status.OK, VVoteNanoHTTPD.MIME_JSON, this.processMessage(
              params.get(ClientConstants.REQUEST_MESSAGE_PARAM)).toString());
        }
        else {
          return new Response(Response.Status.BAD_REQUEST, VVoteNanoHTTPD.MIME_PLAINTEXT, "Message validation failed");
        }
      }
      catch (ProxyException e) {
        logger.error("Exception whilst processing message {}", params.get(ClientConstants.REQUEST_MESSAGE_PARAM), e);
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
    }
    else {
      logger.warn("Missing msg parameter from request");
      return new Response(Response.Status.BAD_REQUEST, VVoteNanoHTTPD.MIME_PLAINTEXT,
          "Missing msg parameter - no message to process");
    }
  }

  /**
   * Perform the actual ballot reduction.
   * 
   * @param ballot
   *          JSONObject of the ballot
   * @param district
   *          JSONObject of district details
   * 
   * 
   * @return JSONArray of the removed permutations
   * @throws JSONException
   */
  public static ArrayList<ArrayList<RemovedCipher>> doBallotReduction(JSONObject ballot, JSONObject district) throws JSONException {
    // Create output JSONArray
    ArrayList<ArrayList<RemovedCipher>> reductions = new ArrayList<ArrayList<RemovedCipher>>();
    int[] maxValues = new int[3];

    // Get race sizes
    maxValues[0] = district.getInt(ConfigFiles.DistrictConfig.DISTRICT_RACE);
    maxValues[1] = district.getInt(ConfigFiles.DistrictConfig.REGION_RACE_ATL);
    maxValues[2] = district.getInt(ConfigFiles.DistrictConfig.REGION_RACE_BTL);

    for (int i = 0; i < maxValues.length; i++) {
      reductions.add(PODProxyServlet.performReduction(ballot.getJSONArray(MessageFields.Ballot.PERMUTATIONS).getJSONArray(i),
          maxValues[i]));
    }

    return reductions;
  }

  /**
   * Removes the permutations that are not required
   * 
   * @param perms
   *          JSONArray of permutations
   * @param maxPerm
   *          integer of total candidates
   * @return ArrayList<RemovedCiphers> an ArrayList of the ciphers that have been removed as part of ballot reduction
   * @throws JSONException
   */
  private static ArrayList<RemovedCipher> performReduction(JSONArray perms, int maxPerm) throws JSONException {
    // Loop through and find all permutations that are greater than the number of candidates. Add them to the list of permutations
    // to remove
    ArrayList<RemovedCipher> remove = new ArrayList<RemovedCipher>();
    for (int i = 0; i < perms.length(); i++) {
      if (perms.getInt(i) >= maxPerm) {
        remove.add(new RemovedCipher(i, perms.getInt(i)));
      }
    }

    // Sort the remove list in reverseOrder. This allows us to remove based on index number without having to adjust the index for
    // removal
    Collections.sort(remove, Collections.reverseOrder());
    for (RemovedCipher rem : remove) {
      perms.remove(rem.getIndex());
    }

    return remove;
  }
}
