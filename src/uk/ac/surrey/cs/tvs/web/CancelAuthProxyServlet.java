/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.web;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.ballotgen.exceptions.ProxyException;
import uk.ac.surrey.cs.tvs.ballotgen.exceptions.ProxyMessageProcessingException;
import uk.ac.surrey.cs.tvs.client.Client;
import uk.ac.surrey.cs.tvs.comms.http.NanoServlet;
import uk.ac.surrey.cs.tvs.comms.http.thirdparty.VVoteNanoHTTPD;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles.ClientConfig;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.CancelAuthMessage;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.io.BoundedBufferedInputStream;
import uk.ac.surrey.cs.tvs.utils.io.JSONUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.StreamBoundExceededException;
import fi.iki.elonen.NanoHTTPD.Method;
import fi.iki.elonen.NanoHTTPD.Response;

/**
 * Servlet used to handle ballot reduction.
 * 
 * @author Chris Culnane
 * 
 * @version $Revision: 1.0 $
 */
public class CancelAuthProxyServlet implements NanoServlet {

  /**
   * Client object that this servlet interacts with
   */
  private Client              client;

  /**
   * TVSKeyStore for holding the client keys
   */
  private TVSKeyStore         clientKeyStore;

  /**
   * Logger
   */
  private static final Logger logger      = LoggerFactory.getLogger(CancelAuthProxyServlet.class);

  /**
   * String to hold the schema in
   */
  private String              schema;

  /**
   * Path to the schema that covers messages received by this proxy
   */
  private static String       SCHEMA_PATH = "/sdcard/vvoteclient/schemas/cancelauthproxyschema.json";

  /**
   * Creates a new PODProxyServlet that references the configFile and districtConfig as specified. This will handle Print on Demand
   * requests from the client HTML.
   * 
   * 
   * @param districtConfFile
   *          path to district configuration file - number of candidates in each race
   * @param client
   *          Client
   * @throws JSONIOException
   * @throws CryptoIOException
   */
  public CancelAuthProxyServlet(Client client, String districtConfFile) throws JSONIOException, CryptoIOException {
    super();

    logger.info("Loading CancelAuthProxyServlet");
    this.client = client;
    
    this.schema = JSONUtils.loadSchema(CancelAuthProxyServlet.SCHEMA_PATH);
    this.clientKeyStore = CryptoUtils.loadTVSKeyStore(this.client.getClientConf().getStringParameter(
        ClientConfig.SYSTEM_CERTIFICATE_STORE));
    logger.info("Finished loading CancelAuthProxyServlet");
  }

  /**
   * Process and incoming message string, which should be in JSON format and with the relevant fields.
   * 
   * @param message
   *          JSON String of a message
   * 
   * 
   * @return JSONObject response
   * @throws ProxyException
   */
  private JSONObject processMessage(String message) throws ProxyException {
    try {
      // We know the type is pod because the schema restricts it

      // Load the message object
      JSONObject messageSent = new JSONObject(message);

      // Add the data to the messageSent - this will be the message that is then sent onto the MBB peers
      // Prepare to sign the contents of the message

      TVSKeyStore tvsKeyStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
      tvsKeyStore.load(this.client.getClientConf().getStringParameter(ClientConfig.SIGNING_KEY_STORE), "".toCharArray());

      TVSSignature submitSig = new TVSSignature(SignatureType.BLS, tvsKeyStore.getBLSPrivateKey(ClientConfig.SIGNING_KEY_ALIAS));
      submitSig.update(messageSent.getString(ClientConstants.UICANCELAUTHMessage.TYPE));
      submitSig.update(messageSent.getString(ClientConstants.UICANCELAUTHMessage.SERIAL_NO));
      submitSig.update(messageSent.getString(ClientConstants.UICANCELAUTHMessage.DISTRICT));

      // Add the signature to the message
      messageSent.put(JSONWBBMessage.SENDER_ID, this.client.getClientConf().getStringParameter(ClientConfig.ID));
      messageSent.put(JSONWBBMessage.SENDER_SIG, submitSig.signAndEncode(EncodingType.BASE64));
      JSONObject response = this.sendToVVA(messageSent);

      // Return the response
      return response;

    }
    catch (JSONException e) {
      throw new ProxyException("Exception whilst processing message", e);
    }
    catch (TVSSignatureException e) {
      throw new ProxyException("Exception whilst processing message", e);
    }
    catch (KeyStoreException e) {
      throw new ProxyException("Exception whilst processing message", e);
    }
    catch (ProxyMessageProcessingException e) {
      throw new ProxyException("Exception whilst processing message", e);
    }
  }

  private JSONObject sendToVVA(JSONObject req) throws ProxyMessageProcessingException, JSONException {
    DefaultHttpClient httpClient = null;
    try {
      logger.info("Preparing to send CancelAuthRequest to VVA");
      req.put(CancelAuthMessage.EVENT_ID, this.client.getClientConf().getStringParameter(ClientConfig.EVENT_ID));
      JSONObject msg = new JSONObject();
      msg.put("Quarantine", req);
      httpClient = new DefaultHttpClient();
      KeyStore trustStore = CryptoUtils.loadKeyStore(this.client.getClientConf().getStringParameter(ClientConfig.VVA_CA_CERT),
          this.client.getClientConf().getStringParameter(ClientConfig.VVA_CA_PW));
      SSLSocketFactory factory = new SSLSocketFactory(trustStore);
      factory.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
      ClientConnectionManager manager = httpClient.getConnectionManager();
      manager.getSchemeRegistry().register(new Scheme("https", factory, 443));

      HttpPost postRequest = new HttpPost(this.client.getClientConf().getStringParameter(ClientConfig.CANCEL_AUTH_ADDRESS));
      logger.info("Sending request to {}",this.client.getClientConf().getStringParameter(ClientConfig.CANCEL_AUTH_ADDRESS));
      StringEntity input = new StringEntity(msg.toString());
      input.setContentType("application/json");
      postRequest.setEntity(input);

      HttpResponse response = httpClient.execute(postRequest);

      if (response.getStatusLine().getStatusCode() == 200) {
        BoundedBufferedInputStream br = null;
        logger.info("Received response: 200");
        try {
          br = new BoundedBufferedInputStream((response.getEntity().getContent()));
          StringBuffer sb = new StringBuffer();
          String line = null;
          while ((line = br.readLine()) != null) {
            sb.append(line);
          }
          JSONObject resp = new JSONObject(sb.toString());
          if (resp.getInt("ErrorCode") == 0 && resp.isNull("Error")) {
            resp.remove("ErrorCode");
            resp.remove("Error");
            return resp;
          }
          else {
            logger.error("Error receieved from VVA: {}", resp);
            return resp;
          }
        }
        catch (StreamBoundExceededException e) {
          throw new ProxyMessageProcessingException(e);
        }
        finally {
          br.close();
        }
      }
      else {
        logger.warn("Failed : HTTP error code : {} ",response.getStatusLine().getStatusCode());
        throw new ProxyMessageProcessingException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
      }
    }
    catch (UnsupportedEncodingException e) {
      throw new ProxyMessageProcessingException(e);
    }
    catch (ClientProtocolException e) {
      throw new ProxyMessageProcessingException(e);
    }
    catch (IOException e) {
      throw new ProxyMessageProcessingException(e);
    }
    catch (CryptoIOException e) {
      throw new ProxyMessageProcessingException(e);
    }
    catch (KeyManagementException e) {
      throw new ProxyMessageProcessingException(e);
    }
    catch (UnrecoverableKeyException e) {
      throw new ProxyMessageProcessingException(e);
    }
    catch (NoSuchAlgorithmException e) {
      throw new ProxyMessageProcessingException(e);
    }
    catch (KeyStoreException e) {
      throw new ProxyMessageProcessingException(e);
    }
    finally {
      if (httpClient != null) {
        httpClient.getConnectionManager().shutdown();
      }
    }

  }

  /**
   * Runs the servlet.
   * 
   * 
   * @param uri
   *          String
   * @param method
   *          Method
   * @param header
   *          Map<String,String>
   * @param params
   *          Map<String,String>
   * @param files
   *          Map<String,String>
   * @param homeDir
   *          File
   * @return Response
   * 
   * @see uk.ac.surrey.cs.tvs.comms.http.NanoServlet#runServlet(java.lang.String, fi.iki.elonen.NanoHTTPD.Method, java.util.Map,
   *      java.util.Map, java.util.Map, java.io.File)
   */
  @Override
  public Response runServlet(String uri, Method method, Map<String, String> header, Map<String, String> params,
      Map<String, String> files, File homeDir) {
    if (params.containsKey(ClientConstants.REQUEST_MESSAGE_PARAM)) {
      try {
        if (JSONUtils.validateSchema(this.schema, params.get(ClientConstants.REQUEST_MESSAGE_PARAM))) {
          return new Response(Response.Status.OK, VVoteNanoHTTPD.MIME_JSON, this.processMessage(
              params.get(ClientConstants.REQUEST_MESSAGE_PARAM)).toString());
        }
        else {
          return new Response(Response.Status.BAD_REQUEST, VVoteNanoHTTPD.MIME_PLAINTEXT, "Message validation failed");
        }
      }
      catch (ProxyException e) {
        logger.error("Exception whilst processing message {}", params.get(ClientConstants.REQUEST_MESSAGE_PARAM), e);
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
    }
    else {
      logger.warn("Missing msg parameter from request");
      return new Response(Response.Status.BAD_REQUEST, VVoteNanoHTTPD.MIME_PLAINTEXT,
          "Missing msg parameter - no message to process");
    }
  }

}
