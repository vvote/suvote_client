/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ui;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.HashMap;

import org.java_websocket.WebSocket;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.ballotgen.exceptions.ClientException;
import uk.ac.surrey.cs.tvs.client.Client;
import uk.ac.surrey.cs.tvs.comms.exceptions.PeerSSLInitException;
import uk.ac.surrey.cs.tvs.comms.http.NanoServlet;
import uk.ac.surrey.cs.tvs.comms.http.VECWebSocket;
import uk.ac.surrey.cs.tvs.comms.http.VECWebSocketException;
import uk.ac.surrey.cs.tvs.comms.http.VECWebSocketListener;
import uk.ac.surrey.cs.tvs.comms.http.VVoteWebServer;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles.ClientConfig;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.exceptions.MaxTimeoutExceeded;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * 
 * ClientUI is a wrapper class to interface between the WebSocket interface and the underlying Client object. It interrogates the
 * Client to determine what needs doing and interfaces with the web based UI to get the necessary files and call the actions.
 * 
 * @author Chris Culnane
 * 
 */
public class ClientUI implements VECWebSocketListener {

  /**
   * VVoteWebServer to use to host the UI web pages
   */
  private VVoteWebServer      sws;

  /**
   * VECWebSocket to listen for WebSocket connections - the UI is WebSocket driven
   */
  private VECWebSocket        vws;

  /**
   * ProgressMonitor to handle receiving progress updates and sending them to the WebSocket
   */
  private ProgressMonitor     pm;

  /**
   * Client object that this ClientUI refers to
   */
  private Client              client;

  /**
   * The address the sockets will listen to - hard coded to localhost, should not be used for listening to non-localhost addresses
   */
  private static final String LISTEN_ADDRESS = "localhost";

  /**
   * Location of javascript file to store the dynamically generated WebSocket port into - this allows the client to dynamically read
   * the WebSocket port when run
   */
  private String              localPortFile;

  /**
   * The folder to serve the UI from
   */
  private String              uiFolder;

  /**
   * Logger
   */
  private static final Logger logger         = LoggerFactory.getLogger(ClientUI.class);

  /**
   * Default constructor for Android device with hard-coded paths
   * 
   * @param clientConf
   *          String to the path of the client configuration file
   * @throws ClientException
   * 
   */
  public ClientUI(String clientConf) throws ClientException {
    this(clientConf, "/sdcard/vvoteclient/vvwww/ui", "./js/localport.json");
  }

  /**
   * Constructor for ClientUI that requires client config path as well as path to UI folder and local port file
   * 
   * @param clientConf
   *          String to the path of the client configuration file
   * @param uiFolderPath
   *          String path to the UI folder
   * @param localPortFilePath
   *          String path to the local port file
   * @throws ClientException
   */
  public ClientUI(String clientConf, String uiFolderPath, String localPortFilePath) throws ClientException {
    super();

    try {
      this.uiFolder = uiFolderPath;
      this.localPortFile = localPortFilePath;
      this.client = new Client(clientConf);
      this.client.start();
      init();

    }
    catch (IOException e) {
      throw new ClientException(e);
    }
    catch (JSONIOException e) {
      throw new ClientException(e);
    }
    catch (PeerSSLInitException e) {
      throw new ClientException(e);
    }
    catch (MaxTimeoutExceeded e) {
      throw new ClientException(e);
    }
  }

  /**
   * @param client
   *          Pre-constructed client object
   * @throws ClientException
   * 
   */
  public ClientUI(Client client) throws ClientException {
    super();
    this.uiFolder="/sdcard/vvoteclient/vvwww/ui";
    this.localPortFile="./js/localport.json";
    this.client = client;
    init();

  }

  /**
   * Initialises the clientUI - including starting the servers
   * 
   * @throws ClientException
   */
  private void init() throws ClientException {
    try {
      CryptoUtils.initProvider();

      this.sws = new VVoteWebServer(ClientUI.LISTEN_ADDRESS, this.client.getClientConf().getIntParameter(
          ConfigFiles.ClientConfig.UI_PORT), new File(uiFolder), new HashMap<String, NanoServlet>());
      this.sws.start();

      InetSocketAddress listenAddress = new InetSocketAddress(ClientUI.LISTEN_ADDRESS, 0);

      this.vws = new VECWebSocket(listenAddress);
      this.vws.addWebSocketListener(this);
      this.vws.start();
      this.vws.waitForStartAndUpdatePort(new File(uiFolder + "/" + localPortFile));
    }
    catch (IOException e) {
      throw new ClientException(e);
    }
    catch (JSONIOException e) {
      throw new ClientException(e);
    }
    catch (VECWebSocketException e) {
      throw new ClientException(e);
    }
    catch (JSONException e) {
      throw new ClientException(e);
    }

  }

  /**
   * Creates and attempts to send an error message to the client. If it fails the exception is caught and logged.
   * 
   * @param resp
   *          JSONObject to place response values in - this will be returned to the client in this method
   * @param message
   *          String message providing more details of the error
   * @param ws
   *          WebSocket to send error message to
   */
  private void processErrorMessage(JSONObject resp, String message, WebSocket ws) {
    try {
      resp.put(ClientConstants.UI.UI_TYPE, ClientConstants.UI.UI_RESP_ERROR);
      resp.put(ClientConstants.UI.UI_RESP_MSG, message);
      ws.send(resp.toString());
    }
    catch (JSONException e) {
      logger.error("Exception whilst trying to send error message to client", e);
    }
  }

  /**
   * Starts the actual ballot generation off and sends back progress updates
   * 
   * @param resp
   *          JSONObject to set response values in
   * @param ws
   *          WebSocket to send progress updates to
   * @throws ClientException
   * @throws JSONException
   */
  private void processGenBallotsRequest(JSONObject resp, WebSocket ws) throws ClientException, JSONException {
    this.pm = new ProgressMonitor(ClientConstants.UI.UI_TYPE_GEN_BALLOTS, ws);
    this.client.addProgressListener(this.pm);
    this.client.generateBallots();
    this.client.removeProgressListener(this.pm);
    resp.put(ClientConstants.UI.UI_TYPE, ClientConstants.UI.UI_RESP_FINISHED);
    resp.put(ClientConstants.UI.UI_RESP_TASK_ID, ClientConstants.UI.UI_TYPE_GEN_BALLOTS);
  }

  /**
   * Generates a crypto key pair and sends progress updates back to the client
   * 
   * @param resp
   *          JSONObject to set response values in
   * @param ws
   *          WebSocket to send progress updates to
   * @throws ClientException
   * @throws JSONException
   */
  private void processGenCryptoKeyRequest(JSONObject resp, WebSocket ws) throws ClientException, JSONException {
    this.pm = new ProgressMonitor(ClientConstants.UI.UI_TYPE_GEN_CRYPTO_KEY, ws);
    this.client.addProgressListener(this.pm);
    this.client.generateCryptoKeyPair();
    this.client.removeProgressListener(this.pm);
    resp.put(ClientConstants.UI.UI_TYPE, ClientConstants.UI.UI_RESP_FINISHED);
    resp.put(ClientConstants.UI.UI_RESP_TASK_ID, ClientConstants.UI.UI_TYPE_GEN_CRYPTO_KEY);
  }

  /**
   * Gets a CSR and returns it to the client
   * 
   * @param resp
   *          JSONObject to set response values in
   * @throws IOException
   * @throws JSONException
   * @throws ClientException
   */
  private void processGetCSRRequest(JSONObject resp) throws IOException, JSONException, ClientException {
    if (this.client.getStatus() > 1) {

      // String csrString = IOUtils.readStringFromFile(this.client.getCSRFileName());
      String csrString = this.client.getJSONPublicKey();
      String sslcsrString = IOUtils.readStringFromFile(this.client.getSSLCSRFileName());
      resp.put(ClientConstants.UI.UI_TYPE, "CSRString");
      resp.put(ClientConstants.UI.UI_RESP_CSR, csrString);
      resp.put(ClientConstants.UI.UI_RESP_SSLCSR, sslcsrString);
    }
    else {
      resp.put(ClientConstants.UI.UI_TYPE, ClientConstants.UI.UI_RESP_ERROR);
      resp.put(ClientConstants.UI.UI_RESP_MSG, "CSR has not been generated yet");
    }
  }

  /**
   * Imports the certificates that have been sent in
   * 
   * @param resp
   *          JSONObject to set response values in
   * @param message
   *          JSONObject representing the incoming message, which should contain the certificates
   * @throws JSONException
   * @throws IOException
   * @throws ClientException
   */
  private void processImportCertsRequest(JSONObject resp, JSONObject message) throws JSONException, IOException, ClientException {
    if (this.client.getStatus() > 1) {
      // String cert = message.getString(ClientConstants.UI.UI_REQ_CERT_STRING);
      String sslCert = message.getString(ClientConstants.UI.UI_REQ_SSLCERT_STRING);
      File certFolder = this.client.getCertsLocation();
      String id = this.client.getClientConf().getStringParameter(ClientConfig.ID);
      // File certLocation = new File(certFolder, id + ClientConstants.CERT_FILE_EXT);
      File sslCertLocation = new File(certFolder, id + ClientConstants.SSLCERT_FILE_SUFFIX + ClientConstants.CERT_FILE_EXT);

      // IOUtils.writeStringToFile(cert, certLocation.getAbsolutePath());
      IOUtils.writeStringToFile(sslCert, sslCertLocation.getAbsolutePath());

      // this.client.importCertificate(certLocation.getAbsolutePath());
      this.client.importSSLCertificate(sslCertLocation.getAbsolutePath());

      resp.put(ClientConstants.UI.UI_TYPE, ClientConstants.UI.UI_RESP_FINISHED);
      resp.put(ClientConstants.UI.UI_RESP_TASK_ID, ClientConstants.UI.UI_TYPE_IMPORT_CERTS);
    }
    else {
      resp.put(ClientConstants.UI.UI_TYPE, ClientConstants.UI.UI_RESP_ERROR);
      resp.put(ClientConstants.UI.UI_RESP_MSG, "CSR has not been generated yet");
    }
  }

  /**
   * Processes a message and calls the relevant methods in the Client
   * 
   * @param message
   *          JSONObject of the message
   * @param ws
   *          WebSocket to send response to
   * 
   * @see uk.ac.surrey.cs.tvs.comms.http.VECWebSocketListener#processMessage(org.json.JSONObject, org.java_websocket.WebSocket)
   */
  @Override
  public void processMessage(JSONObject message, WebSocket ws) {
    JSONObject resp = new JSONObject();
    try {
      String type = message.getString(ClientConstants.UI.UI_TYPE);
      if (type.equalsIgnoreCase(ClientConstants.UI.UI_TYPE_GET_STATUS)) {
        this.processStatusRequest(resp);
      }
      else if (type.equalsIgnoreCase(ClientConstants.UI.UI_TYPE_GET_TYPE)) {
        this.processTypeRequest(resp);
      }
      else if (type.equalsIgnoreCase(ClientConstants.UI.UI_TYPE_GEN_SIGNING_KEY)) {
        this.processsGenSigningKeyRequest(resp, ws);
      }
      else if (type.equalsIgnoreCase(ClientConstants.UI.UI_TYPE_GET_CSR)) {
        this.processGetCSRRequest(resp);
      }
      else if (type.equalsIgnoreCase(ClientConstants.UI.UI_TYPE_IMPORT_CERTS)) {
        this.processImportCertsRequest(resp, message);
      }
      else if (type.equalsIgnoreCase(ClientConstants.UI.UI_TYPE_GEN_CRYPTO_KEY)) {
        this.processGenCryptoKeyRequest(resp, ws);
      }
      else if (type.equalsIgnoreCase(ClientConstants.UI.UI_TYPE_GEN_BALLOTS)) {
        this.processGenBallotsRequest(resp, ws);
      }
      else {
        this.processUnknownMessage(resp);
      }
      ws.send(resp.toString());
    }
    catch (JSONException e) {
      logger.error("Exception whilst processing ClientUI message", e);
      this.processErrorMessage(resp, "Error whilst processing request:" + e.getMessage(), ws);
    }
    catch (IOException e) {
      logger.error("Exception whilst processing ClientUI message", e);
      this.processErrorMessage(resp, "Error whilst processing request:" + e.getMessage(), ws);
    }
    catch (ClientException e) {
      logger.error("Exception whilst processing ClientUI message", e);
      this.processErrorMessage(resp, "Error whilst processing request:" + e.getMessage(), ws);
    }
  }

  /**
   * Generates a signing key and prepares the progress listeners
   * 
   * @param resp
   *          JSONObject to response values in
   * @param ws
   *          WebSocket to send progress updates to
   * @throws ClientException
   * @throws JSONException
   */
  private void processsGenSigningKeyRequest(JSONObject resp, WebSocket ws) throws ClientException, JSONException {
    this.pm = new ProgressMonitor(ClientConstants.UI.UI_TYPE_GEN_SIGNING_KEY, ws);
    this.client.addProgressListener(this.pm);
    this.client.generateSigningKey();
    this.client.removeProgressListener(this.pm);
    resp.put(ClientConstants.UI.UI_TYPE, ClientConstants.UI.UI_RESP_FINISHED);
    resp.put(ClientConstants.UI.UI_RESP_TASK_ID, ClientConstants.UI.UI_TYPE_GEN_SIGNING_KEY);
  }

  /**
   * Gets the status of this client
   * 
   * @param resp
   *          JSONObject of the response to set value in
   * @throws JSONException
   */
  private void processStatusRequest(JSONObject resp) throws JSONException {
    resp.put(ClientConstants.UI.UI_TYPE, ClientConstants.UI.UI_RESP_TYPE_STATUS);
    if (this.client.isInitialised()) {
      resp.put(ClientConstants.UI.UI_RESP_TYPE_STATUS, Client.INITIALISED_PARAM);
    }
    else {
      resp.put(ClientConstants.UI.UI_RESP_TYPE_STATUS, this.client.getNextTask());
    }
  }

  /**
   * Gets the type of client this is
   * 
   * @param resp
   *          JSONObject of the response to set value in
   * @throws JSONException
   */
  private void processTypeRequest(JSONObject resp) throws JSONException {
    resp.put(ClientConstants.UI.UI_TYPE, ClientConstants.UI.UI_RESP_TYPE_TYPE);
    resp.put(ClientConstants.UI.UI_RESP_TYPE_TYPE, this.client.getType().toString());
  }

  /**
   * Process an unknown message type
   * 
   * @param resp
   *          JSONObject to set response values in
   * @throws JSONException
   */
  private void processUnknownMessage(JSONObject resp) throws JSONException {
    resp.put(ClientConstants.UI.UI_TYPE, ClientConstants.UI.UI_RESP_ERROR);
    resp.put(ClientConstants.UI.UI_RESP_MSG, "Unknown message type");
  }

  /**
   * Main method to call this from the CLI
   * 
   * @param args
   *          String array of arguments - should a single element that is the path to the config file
   * @throws ClientException
   */
  public static void main(String[] args) throws ClientException {
    if (args.length != 1) {
      printUsage();
    }
    else {
      new ClientUI(args[0]);
    }
  }

  /**
   * Prints the usage instructions to the System.out
   */
  private static final void printUsage() {
    System.out.println("Usage of ClientUI:");
    System.out.println("ClientUI starts the web server that will serve");
    System.out.println("the user interface for the ClientUI. A configuration");
    System.out.println("file is required. ");
    System.out.println("Usage:\n");
    System.out.println("ClientUI [pathToConfigFile] \n");
    System.out.println("\tpathToConfigFile: path to configuration file.");
  }

  public void shutdown() {
    try {
      this.vws.stop();
    }
    catch (IOException e) {
      logger.error("Exception whilst shutting down VEC WebSocket", e);
    }
    catch (InterruptedException e) {
      logger.error("Exception whilst shutting down VEC WebSocket", e);
    }
    this.sws.stop();
  }
}
