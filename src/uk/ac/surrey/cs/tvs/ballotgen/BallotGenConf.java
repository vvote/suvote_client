/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen;

import org.json.JSONArray;
import org.json.JSONException;

import uk.ac.surrey.cs.tvs.comms.exceptions.PeerSSLInitException;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles.BallotGenConfig;
import uk.ac.surrey.cs.tvs.utils.io.ConfigFile;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * Provides an extension of the standard ConfigFile with some Ballot Generation specified utility methods.
 * 
 * @author Chris Culnane
 * 
 */
public class BallotGenConf extends ConfigFile {

  /**
   * Construct the ConfigFile - if no file is specified it will utilise the default file as set in ConfigFile.
   * 
   * @throws JSONIOException
   * @throws PeerSSLInitException
   */
  public BallotGenConf() throws JSONIOException {
    super();
  }

  /**
   * Construct a BallotGenConf object using the JSON stored in the file specified by configFile.
   * 
   * @param configFile
   *          String path to config file.
   * @throws JSONIOException
   * @throws PeerSSLInitException
   */
  public BallotGenConf(String configFile) throws JSONIOException {
    super(configFile);
  }

  /**
   * Returns an int array that contains the number of candidates in each race.
   * 
   * @return int array containing the number of candidates in each race.
   * @throws JSONException
   */
  public int[] getRaceCounts() throws JSONException {

    JSONArray races = this.conf.getJSONArray(BallotGenConfig.RACES);
    int[] raceCounts = new int[races.length()];

    for (int i = 0; i < races.length(); i++) {
      raceCounts[i] = races.getJSONObject(i).getInt(BallotGenConfig.RACE_NUM_CANDIDATES);
    }

    return raceCounts;
  }
}
