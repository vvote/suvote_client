/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.ballotgen.exceptions.EndOfFileException;
import uk.ac.surrey.cs.tvs.ballotgen.exceptions.RandomnessCommitmentException;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CommitException;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * Represents a row of randomness data to be used during the ballot generation. This only represents the randomness from a single
 * source. The rows from different sources are combined before being applied.
 * 
 * @author Chris Culnane
 * 
 */
public class RandomnessRow {

  /**
   * JSONObject representing the contents of a row from a source of randomness
   */
  private JSONObject          row;

  /**
   * JSONObject representing the contents of a row of commit values from a source of randomness
   */
  private JSONObject          commitRow;

  /**
   * JSONArray of the actual commit values from the commitRow
   */
  private JSONArray           commits;

  /**
   * JSONArray of the actual randomness value from the row
   */
  private JSONArray           randValues;

  /**
   * Index to keep track of where we are in the arrays
   */
  private int                 currentIndex = 0;

  /**
   * Cipher constructed to decrypt using AES session key
   */
  private Cipher              decrypt;

  /**
   * Logger
   */
  private static final Logger logger       = LoggerFactory.getLogger(RandomnessRow.class);

  /**
   * Construct a RandomnessRow from the supplied values.
   * 
   * @param rowString
   *          String containing a JSONObject that represents a row
   * @param decrypt
   *          Cipher object setup to perform decryption using AES session key
   * @param commitString
   *          String containing a JSONObject that represents the commit values for a row
   * @throws JSONException
   * @throws EndOfFileException 
   */
  public RandomnessRow(String rowString, Cipher decrypt, String commitString) throws JSONException, EndOfFileException {
    super();
    if(rowString==null || commitString==null){
      throw new EndOfFileException("Randomness row values are null");
    }
    this.row = new JSONObject(rowString);

    // Get the actual randomness array
    this.randValues = this.row.getJSONArray(MessageFields.RandomnessFile.RANDOMNESS);
    this.decrypt = decrypt;
    this.commitRow = new JSONObject(commitString);

    // Get the actual commit randomness values
    this.commits = this.commitRow.getJSONArray(MessageFields.RandomnessFile.RANDOMNESS);
  }

  /**
   * Returns the JSONObject containing the randomness data for this row. This is utilised during an audit.
   * 
   * @return JSONObject containing row data, including randomness values
   * 
   * @throws RandomnessCommitmentException
   */
  public JSONObject getAuditData() throws RandomnessCommitmentException {
    try {
      JSONObject auditRow = new JSONObject(this.row.toString());
      JSONArray randVals = auditRow.getJSONArray(MessageFields.RandomnessFile.RANDOMNESS);

      for (int i = 0; i < randVals.length(); i++) {
        randVals.getJSONObject(i).put(
            MessageFields.RandomnessFile.RANDOM_VALUE,
            IOUtils.encodeData(
                EncodingType.HEX,
                this.decrypt.doFinal(IOUtils.decodeData(EncodingType.HEX,
                    randVals.getJSONObject(i).getString(MessageFields.RandomnessFile.RANDOM_VALUE)))));
        randVals.getJSONObject(i).put(
            MessageFields.RandomnessFile.RANDOM_VALUE_COMMIT,
            IOUtils.encodeData(
                EncodingType.HEX,
                this.decrypt.doFinal(IOUtils.decodeData(EncodingType.HEX,
                    randVals.getJSONObject(i).getString(MessageFields.RandomnessFile.RANDOM_VALUE_COMMIT)))));

      }

      return auditRow;
    }
    catch (IllegalBlockSizeException e) {
      throw new RandomnessCommitmentException("Exception occured whilst decrypting audit data", e);
    }
    catch (BadPaddingException e) {
      throw new RandomnessCommitmentException("Exception occured whilst decrypting audit data", e);
    }
    catch (JSONException e) {
      throw new RandomnessCommitmentException("Exception occured whilst decrypting audit data", e);
    }
  }

  /**
   * Get the next randomness value, first checking that the randomness value in file is consistent with the commit value.
   * 
   * @return byte array of random bytes from the file if consistent, otherwise an exception is thrown
   * @throws RandomnessCommitmentException
   */
  public byte[] getNextValue() throws RandomnessCommitmentException {
    try {
      // Retrieve and decrypt the randomness value
      byte[] randVal = this.decrypt.doFinal(IOUtils.decodeData(EncodingType.HEX, this.randValues.getJSONObject(this.currentIndex)
          .getString(MessageFields.RandomnessFile.RANDOM_VALUE)));

      // Retrieve and decrypt the corresponding commitment value
      byte[] commitVal = this.decrypt.doFinal(IOUtils.decodeData(EncodingType.HEX, this.randValues.getJSONObject(this.currentIndex)
          .getString(MessageFields.RandomnessFile.RANDOM_VALUE_COMMIT)));

      // Decode the commitment sent to the MBB from the commit file
      byte[] publicCommit = IOUtils.decodeData(EncodingType.HEX, this.commits.getString(this.currentIndex));

      // Check the constructed commitment is the same as the submitted commitment
      if (!CryptoUtils.checkCommitment(publicCommit, commitVal, randVal)) {
        logger.error("Supplied randomness is not consistent with commitment");
        throw new RandomnessCommitmentException("Supplied randomness is not consistent with commitment");
      }
      this.currentIndex++;

      return randVal;
    }
    catch (IllegalBlockSizeException e) {
      throw new RandomnessCommitmentException("Exception occured whilst retrieving and checking randomness", e);
    }
    catch (BadPaddingException e) {
      throw new RandomnessCommitmentException("Exception occured whilst retrieving and checking randomness", e);
    }
    catch (JSONException e) {
      throw new RandomnessCommitmentException("Exception occured whilst retrieving and checking randomness", e);
    }
    catch (CommitException e) {
      throw new RandomnessCommitmentException("Exception occured whilst retrieving and checking randomness", e);
    }
  }

  /**
   * Gets the serialNo associated with this row of randomness data.
   * 
   * @return String containing the serialNo
   * @throws JSONException
   */
  public String getSerialNo() throws JSONException {
    return this.row.getString(MessageFields.RandomnessFile.SERIAL_NO);
  }

  /**
   * Sets the peerID value in the row JSONObject this is needed to distinguish the source of the different rows.
   * 
   * @param id
   *          String containing the peerID
   * @throws JSONException
   */
  public void setPeerID(String id) throws JSONException {
    this.row.put(MessageFields.RandomnessFile.PEER_ID, id);
  }
}
