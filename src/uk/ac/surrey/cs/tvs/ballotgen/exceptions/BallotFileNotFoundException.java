/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen.exceptions;

/**
 * Exception used when a ballot file cannot be found, either during Print on Demand or during an audit.
 * 
 * @author Chris Culnane
 * 
 */
public class BallotFileNotFoundException extends Exception {

  /**
   * For serialisation.
   */
  private static final long serialVersionUID = 1267678551362109034L;

  /**
   * Default constructor.
   */
  public BallotFileNotFoundException() {
    super();
  }

  /**
   * Constructor which allows the message to be defined.
   * 
   * @param message
   *          The exception message.
   */
  public BallotFileNotFoundException(String message) {
    super(message);
  }

  /**
   * Constructor which allows the message and cause to be defined.
   * 
   * @param message
   *          The exception message.
   * @param cause
   *          The cause of the exception.
   */
  public BallotFileNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * Constructor which allows the message and cause to be defined and other optional parameters.
   * 
   * @param message
   *          The exception message.
   * @param cause
   *          The cause of the exception.
   * @param enableSuppression
   *          Whether or not suppression is enabled or disabled.
   * @param writableStackTrace
   *          Whether or not the stack trace should be writable.
   */
  public BallotFileNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  /**
   * Constructor which allows the cause to be defined.
   * 
   * @param cause
   *          The cause of the exception.
   */
  public BallotFileNotFoundException(Throwable cause) {
    super(cause);
  }

}
