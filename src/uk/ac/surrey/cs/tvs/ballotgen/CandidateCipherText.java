/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen;

import java.math.BigInteger;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONException;
import org.json.JSONObject;

import uk.ac.surrey.cs.tvs.utils.crypto.ECUtils;

/**
 * Class that represents a CandidateID Cipher text. This is the class where the actual crypto is performed. It also provides the
 * comparable interface to allow a list of ciphertext to be sorted, which is part of the random permuting utilised by the mix.
 * 
 * @author Chris Culnane
 * 
 */
public class CandidateCipherText implements Comparable<CandidateCipherText>, Runnable {

  /**
   * ECPoint array containing the alpha and beta parts of a cipher text
   */
  private ECPoint[]  cipher;

  /**
   * A BigInteger cache of the g^r value
   */
  private BigInteger grCache;

  /**
   * A BigInteger cache of the m*y^r value
   */
  private BigInteger myrCache;

  /**
   * ECGroup representing the underlying group
   */
  private ECUtils    group;

  /**
   * Integer of the permutation index - used during the permuting process
   */
  private int        permutationIndex;

  /**
   * Randomness used for re-encryption - only used during deferred re-encryption
   */
  private BigInteger rand;

  /**
   * ECPoint representing the public key - only used during deferred re-encryption
   */
  private ECPoint    pk;

  /**
   * Construct a CandidateCipherText from the JSONObject with the specified underlying ECGroup
   * 
   * @param group
   *          ECGroup representing underlying group
   * @param jCipher
   *          JSONObject containing a candidate cipher text
   * @throws JSONException
   */
  public CandidateCipherText(ECUtils group, JSONObject jCipher) throws JSONException {
    super();

    this.cipher = group.jsonToCipher(jCipher);
    this.group = group;

    this.updateCache();
  }

  /**
   * Compares this CandidateCipherText with another. This is utilised to sort the re-encrypted cipher texts, thus creating a random
   * permutation.
   * 
   * @param o
   *          the object to be compared.
   * @return a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified
   *         object.
   * 
   * @see java.lang.Comparable#compareTo(java.lang.Object)
   */
  @Override
  public int compareTo(CandidateCipherText o) {
    // Check if the g^r is equal, if it is do a compare on the my^r (highly unlikely that the g^r is equal)
    if (this.getGREncoded().equals(o.getGREncoded())) {
      return this.getMYREncoded().compareTo(o.getMYREncoded());
    }
    else {
      // Compare the g^r
      return this.getGREncoded().compareTo(o.getGREncoded());
    }
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((grCache == null) ? 0 : grCache.hashCode());
    result = prime * result + ((myrCache == null) ? 0 : myrCache.hashCode());
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (!(obj instanceof CandidateCipherText)) {
      return false;
    }
    CandidateCipherText other = (CandidateCipherText) obj;
    if (grCache == null) {
      if (other.grCache != null) {
        return false;
      }
    }
    else if (!grCache.equals(other.grCache)) {
      return false;
    }
    if (myrCache == null) {
      if (other.myrCache != null) {
        return false;
      }
    }
    else if (!myrCache.equals(other.myrCache)) {
      return false;
    }
    return true;
  }

  /**
   * Converts the ECPoint array to a JSONObject.
   * 
   * @return JSONObject containing cipher text
   * @throws JSONException
   */
  public JSONObject getAsJSON() throws JSONException {
    return this.group.cipherToJSON(this.cipher);
  }

  /**
   * Gets the combined cipher value.
   * 
   * @return ECPoint array of alpha and beta points of ElGamal cipher
   */
  public ECPoint[] getCipher() {
    return this.cipher;
  }

  /**
   * Gets the BigInteger g^r cache value (encoded ECPoint).
   * 
   * @return BigInteger containing encoded g^r ECPoint
   */
  public BigInteger getGREncoded() {
    return this.grCache;
  }

  /**
   * Gets the BigInteger my^r cache value (encoded ECPoint).
   * 
   * @return BigInteger containing encoded my^r ECPoint
   */
  public BigInteger getMYREncoded() {
    return this.myrCache;
  }

  /**
   * Get the permutation index
   * 
   * @return integer containing the permutation index
   */
  public int getPermutationIndex() {
    return this.permutationIndex;
  }

  /**
   * Get the randomness used for the re-encryption.
   * 
   * @return BigInteger containing the randomness
   */
  public BigInteger getRandValue() {
    return this.rand;
  }

  /**
   * Perform a re-encryption using the public key and randomness specified. The public key should be the correct key associated with
   * the underlying cipher.
   * 
   * @param pk
   *          The public key for the re-encryption
   * @param rand
   *          The new randomness
   */
  public void reencrypt(ECPoint pk, BigInteger rand) {
    this.rand = rand;
    this.cipher=group.reencrypt(cipher, pk, rand);
    //this.cipher[0] = this.cipher[0].add(this.group.getG().multiply(rand));
    //this.cipher[1] = this.cipher[1].add(pk.multiply(rand));

    // The underlying ECPoints have changed.
    this.updateCache();
  }

  /**
   * Performs the re-encryption on the thread.
   * 
   * @see java.lang.Runnable#run()
   */
  @Override
  public void run() {
    this.reencrypt(this.pk, this.rand);
  }

  /**
   * Set the permutation index - done before we sort - which allows tracking the permutation efficiently
   * 
   * @param index
   *          integer of index from 0 - n
   */
  public void setPermutationIndex(int index) {
    this.permutationIndex = index;
  }

  /**
   * Provides a deferred re-encryption method to allow it to be easily multi-threaded. This sets the data that will be used for the
   * re-encryption but doesn't actually perform it.
   * 
   * @param pk
   *          ECPoint representing the public key
   * @param rand
   *          BigInteger with randomness value to use for re-encryption
   */
  public void setReencryptData(ECPoint pk, BigInteger rand) {
    this.pk = pk;
    this.rand = rand;
  }

  /**
   * Pre-loads the g^r and m*y^r cache values from the cipher. This is needed to make the sorting efficient. The sorting needs to
   * access the underlying g^r and my^r values, which if called directly on the ECPoints would cause a new encoding operation to be
   * run each time. Hence we cache them.
   * 
   * updateCache must be called after any change is made to the underlying ECPoints.
   */
  private void updateCache() {
    this.grCache = new BigInteger(1,this.cipher[0].getEncoded());
    this.myrCache = new BigInteger(1,this.cipher[1].getEncoded());
  }
}
