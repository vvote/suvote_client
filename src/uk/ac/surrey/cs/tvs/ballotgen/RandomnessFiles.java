/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.ballotgen.exceptions.ClientException;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles.ClientConfig;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles.RandomnessConfig;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.FileMessage;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStoreException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.io.ConfigFile;
import uk.ac.surrey.cs.tvs.utils.io.DirectoryFilter;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * Utility class to recursively look for directories containing randomness data. Each server will have provided a zip file, that
 * should have been unzipped into its own sub-directory. This code will search through those directories finding each set of
 * randomness data. The determinant is if the folder contains a file called conf.json. This file should contain fields that provide
 * the file names of the relevant files, like the randomness and encrypted aes key.
 * 
 * The data in the lists is indexed consistently. Thus the value in index 1 in aesKeys is the encrypted AESKey associated with the
 * randomness file at index 1 of the randomnessFile list
 * 
 * @author Chris Culnane
 * 
 */
public class RandomnessFiles {

  /**
   * Logger
   */
  private static final Logger logger          = LoggerFactory.getLogger(RandomnessFiles.class);

  /**
   * String of top-level directory to start search in
   */
  private String              inputDir;

  /**
   * List of config objects
   */
  private List<JSONObject>    configs         = new ArrayList<JSONObject>();

  /**
   * List of aesKey file locations
   */
  private List<String>        aesKeys         = new ArrayList<String>();

  /**
   * List of randomness file locations
   */
  private List<String>        randomnessFiles = new ArrayList<String>();

  /**
   * List of commitment file locations
   */
  private List<String>        commitFiles     = new ArrayList<String>();

  /**
   * List of peer IDs
   */
  private List<String>        peerIDS         = new ArrayList<String>();

  /**
   * Construct RandomnessFiles object with associate top level directory
   * 
   * @param inputDir
   *          String containing path to top-level directory to start search in
   */
  public RandomnessFiles(String inputDir) {
    super();

    this.inputDir = inputDir;
  }

  /**
   * Check if the received randomness config is valid and correctly signed
   * 
   * @param clientConf
   *          ConfigFile of the client configuration
   * @param conf
   *          JSONObject containing the Randomness File configuration (received from the RandomnessServer)
   * @param commitFile
   *          String path to commitFile
   * @return boolean - true if file is valid, false if invalid or throws and exception whilst processing
   */
  private boolean checkRandFileIsValid(ConfigFile clientConf, JSONObject conf, String commitFile) {
    try {
      logger.info("Checking file from {}", conf.getString(RandomnessConfig.PEER_ID));
      // Get the Signatures
      String wbbSigs = conf.getString(RandomnessConfig.WBBSIG);
      // Get the actual message
      JSONObject wbbMsg = conf.getJSONObject(RandomnessConfig.WBBMSG);

      // Create a digest for the files
      MessageDigest md = MessageDigest.getInstance(ClientConstants.DEFAULT_MESSAGE_DIGEST);

      IOUtils.addFileIntoDigest(new File(commitFile), md);
      String commitTime = conf.getString(MessageFields.PeerResponse.COMMIT_TIME);

      // Create a new signature object for verifying the signature
      TVSSignature tvsSig = new TVSSignature(SignatureType.BLS, CryptoUtils.loadTVSKeyStore(
          clientConf.getStringParameter(ClientConfig.SYSTEM_CERTIFICATE_STORE)).getBLSCertificate("WBB"));
      tvsSig.update(wbbMsg.getString(FileMessage.ID));
      tvsSig.update(conf.getString(RandomnessConfig.PEER_ID));
      tvsSig.update(clientConf.getStringParameter(ClientConfig.ID));
      tvsSig.update(IOUtils.encodeData(EncodingType.BASE64, md.digest()));
      tvsSig.update(commitTime);

      // Verify the signature

      if (tvsSig.verify(wbbSigs, EncodingType.BASE64)) {
        logger.info("File is valid");
        return true;
      }
      else {
        logger.info("File is invalid");
        return false;
      }
    }
    catch (JSONException e) {
      logger.warn("Received randomness file caused an exception", e);
      return false;
    }
    catch (NoSuchAlgorithmException e) {
      logger.warn("Received randomness file caused an exception", e);
      return false;
    }
    catch (IOException e) {
      logger.warn("Received randomness file caused an exception", e);
      return false;
    }
    catch (TVSSignatureException e) {
      logger.warn("Received randomness file caused an exception", e);
      return false;
    }
    catch (CryptoIOException e) {
      logger.warn("Received randomness file caused an exception", e);
      return false;
    }

    catch (TVSKeyStoreException e) {
      logger.warn("Received randomness file caused an exception", e);
      return false;
    }
  }

  /**
   * Finds all the randomness files.
   * 
   * @throws JSONIOException
   * @throws JSONException
   */
  public void findRandomnessFiles() throws JSONIOException, JSONException {
    logger.info("Resetting randomness files");

    this.aesKeys.clear();
    this.randomnessFiles.clear();
    this.commitFiles.clear();
    this.peerIDS.clear();

    File inputFolder = new File(this.inputDir);
    logger.info("About to start search in {}", this.inputDir);
    File[] files = inputFolder.listFiles(new DirectoryFilter());
    
    if (files != null) {
      Arrays.sort(files);
      for (File f : files) {
        File confFile = new File(f, RandomnessConfig.RANDOMNESS_CONFIG_FILE);

        if (confFile.exists()) {
          // We have found a conf.json, read it and add the data to the lists
          logger.info("Found {} in {}", RandomnessConfig.RANDOMNESS_CONFIG_FILE, f.getAbsolutePath());
          JSONObject conf = IOUtils.readJSONObjectFromFile(confFile.getAbsolutePath());
          this.configs.add(conf);

          this.commitFiles
              .add(f.getAbsolutePath() + File.separator + conf.getString(ConfigFiles.RandomnessConfig.COMMIT_DATA_FILE));
          this.aesKeys.add(f.getAbsolutePath() + File.separator + conf.getString(ConfigFiles.RandomnessConfig.AES_KEY_FILE));
          this.randomnessFiles.add(f.getAbsolutePath() + File.separator
              + conf.getString(ConfigFiles.RandomnessConfig.RANDOMNESS_DATA_FILE));
          this.peerIDS.add(conf.getString(ConfigFiles.RandomnessConfig.PEER_ID));
        }

      }
    }
  }

  /**
   * Gets the list of AESKeys.
   * 
   * @return list of AESKeys found
   */
  public List<String> getAESKeys() {
    return this.aesKeys;
  }

  /**
   * Gets the list of commit files.
   * 
   * @return list of commit files found
   */
  public List<String> getCommitFiles() {
    return this.commitFiles;
  }

  /**
   * Gets the list of PeerIDs.
   * 
   * @return list of PeerIDs found
   */
  public List<String> getPeerIDS() {
    return this.peerIDS;
  }

  /**
   * Gets the list of randomness files.
   * 
   * @return list of randomness files found
   */
  public List<String> getRandomnessFiles() {
    return this.randomnessFiles;
  }

  /**
   * Checks the randomness files that have been received for validity (i.e. that the MBB signature is valid on the data received).
   * Any invalid files will be removed from the list. Returns true if all files are valid, false if enough files (threshold) are
   * valid and throws an exception if insufficient files are valid.
   * 
   * @param clientConf
   *          ConfigFile of the client config to check data is appropriate for this device
   * @return true if all valid, false if threshold valid, exception if insufficient valid or multiple copies from one peer
   * @throws ClientException
   */
  public boolean verifyAllFilesAndSignatures(ConfigFile clientConf) throws ClientException {
    logger.info("Verifying received randomness files");

    // Loop through all files to check whether they are valid or not
    ArrayList<Integer> invalidFiles = new ArrayList<Integer>();

    // Create a hashmap to index the files by ID. We won't use the actual index, we will just use it to check unique senders.
    HashMap<String, JSONObject> peerFiles = new HashMap<String, JSONObject>();
    for (int i = 0; i < this.configs.size(); i++) {
      JSONObject conf = this.configs.get(i);

      if (!this.checkRandFileIsValid(clientConf, conf, this.commitFiles.get(i))) {
        invalidFiles.add(i);
      }
      else {
        try {
          // Add it to the HashMap - this is just used to check with have different sources of randomness, and not n copies from the
          // same peer - PeerID is included in the signature, so the fact this has a combined MBB signature is proof it was sent by
          // the peer in PeerID
          if(!peerFiles.containsKey(conf.getString(RandomnessConfig.PEER_ID))){
            peerFiles.put(conf.getString(RandomnessConfig.PEER_ID), conf);
          }else{
            throw new ClientException("The same peer has sent multiple randomness files");
          }
        }
        catch (JSONException e) {
          // If we couldn't read the peerID something was wrong - shouldn't have got here, since it should have been caught in
          // checkRandFileIsValid, but just in case we will put it in the invalid list
          logger.warn("JSON exception whilst trying to add randomness file to valid list - should have been caught before now", e);
          invalidFiles.add(i);
        }
      }
    }

    //Check if the unique files is large enough
    if (peerFiles.keySet().size() >= clientConf.getIntParameter(ClientConfig.RANDOMNESS_THRESHOLD)) {
      logger.info("Sufficient randomess files are valid");
      Collections.sort(invalidFiles, Collections.reverseOrder());

      for (int rem : invalidFiles) {
        logger.info("Removing invalid file from index {}", rem);
        this.configs.remove(rem);
        this.aesKeys.remove(rem);
        this.commitFiles.remove(rem);
        this.peerIDS.remove(rem);
        this.randomnessFiles.remove(rem);
      }

      if (invalidFiles.size() == 0) {
        return true;
      }
      else {
        return false;
      }
    }
    else {
      logger.info("Only found {} unique randomness sources", peerFiles.keySet().size());
      throw new ClientException("Insufficient valid randomness files");
    }
  }
}
