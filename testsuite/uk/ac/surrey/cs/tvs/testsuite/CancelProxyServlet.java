/**
 * 
 */
package uk.ac.surrey.cs.tvs.testsuite;

import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.ballotgen.exceptions.ProxyMessageProcessingException;
import uk.ac.surrey.cs.tvs.comms.exceptions.MBBCommunicationException;
import uk.ac.surrey.cs.tvs.comms.http.NanoServlet;
import uk.ac.surrey.cs.tvs.comms.http.thirdparty.VVoteNanoHTTPD;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSPrivateKey;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;
import fi.iki.elonen.NanoHTTPD.Method;
import fi.iki.elonen.NanoHTTPD.Response;

/**
 * TestSuite Servlet to receive a cancel request and sign it. Used to simulate a central authorisation signature
 * 
 * @author Chris Culnane
 * 
 */
public class CancelProxyServlet implements NanoServlet {

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(CancelProxyServlet.class);

  /**
   * Private signing key
   */
  private BLSPrivateKey          sigKey;

  /**
   * Construct a new CancelProxyServlet using the key in KeyFile
   * 
   * @param keyFile
   *          location of Java KeyStore containing a suitable private key 
   */
  public CancelProxyServlet(String keyFile) {

    try {
      TVSKeyStore tvsKey = TVSKeyStore.getInstance(KeyStore.getDefaultType());
      tvsKey.load(keyFile, "".toCharArray());
      sigKey =tvsKey.getBLSPrivateKey("CancelAuth");
      //sigKey = CryptoUtils.getSignatureKeyFromKeyStore(keyFile, "", "CancelAuth", "");
      logger.info("Created CancelProxyServlet");
    }
   
    catch (KeyStoreException e) {
      logger.error("Exception whilst creating CancelProxyServlet", e);
    }
    
  }


  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.surrey.cs.tvs.comms.http.NanoServlet#runServlet(java.lang.String, uk.ac.surrey.cs.tvs.comms.http.NanoHTTPD.Method,
   * java.util.Map, java.util.Map, java.util.Map, java.io.File)
   */
  @Override
  public Response runServlet(String uri, Method method, Map<String, String> header, Map<String, String> params,
      Map<String, String> files, File homeDir) {

    if (params.containsKey("msg")) {
      try {
        logger.info("Cancel Proxy received {}", params.containsKey("msg"));
        return new Response(Response.Status.OK, VVoteNanoHTTPD.MIME_JSON, processMessage(params.get("msg")).toString());
      }
      catch (UnrecoverableKeyException e) {
        logger.error("Exception whislt processing CancelProxyServlet message", e);
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
      catch (InvalidKeyException e) {
        logger.error("Exception whislt processing CancelProxyServlet message", e);
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
      catch (KeyStoreException e) {
        logger.error("Exception whislt processing CancelProxyServlet message", e);
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
      catch (NoSuchAlgorithmException e) {
        logger.error("Exception whislt processing CancelProxyServlet message", e);
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
      catch (CertificateException e) {
        logger.error("Exception whislt processing CancelProxyServlet message", e);
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
      catch (SignatureException e) {
        logger.error("Exception whislt processing CancelProxyServlet message", e);
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
      catch (JSONException e) {
        logger.error("Exception whislt processing CancelProxyServlet message", e);
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
      catch (IOException e) {
        logger.error("Exception whislt processing CancelProxyServlet message", e);
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
      catch (CryptoIOException e) {
        logger.error("Exception whislt processing CancelProxyServlet message", e);
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
      catch (TVSSignatureException e) {
        logger.error("Exception whislt processing CancelProxyServlet message", e);
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
      catch (JSONIOException e) {
        logger.error("Exception whislt processing CancelProxyServlet message", e);
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
      catch (MBBCommunicationException e) {
        logger.error("Exception whislt processing CancelProxyServlet message", e);
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
      catch (ProxyMessageProcessingException e) {
        logger.error("Exception whislt processing CancelProxyServlet message", e);
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
    }
    else {
      logger.warn("Invalid message sent to CancelProxyServlet");
      return new Response(Response.Status.BAD_REQUEST, VVoteNanoHTTPD.MIME_PLAINTEXT, "Missing msg parameter - no message to process");
    }

  }

  /**
   * Process the cancellation request
   * @param message String containing JSONObject of message
   * @return JSONObject of response
   * @throws JSONException
   * @throws IOException
   * @throws KeyStoreException
   * @throws NoSuchAlgorithmException
   * @throws CertificateException
   * @throws UnrecoverableKeyException
   * @throws InvalidKeyException
   * @throws SignatureException
   * @throws CryptoIOException
   * @throws TVSSignatureException
   * @throws JSONIOException
   * @throws MBBCommunicationException
   * @throws ProxyMessageProcessingException
   */
  private JSONObject processMessage(String message) throws JSONException, IOException, KeyStoreException, NoSuchAlgorithmException,
      CertificateException, UnrecoverableKeyException, InvalidKeyException, SignatureException, CryptoIOException,
      TVSSignatureException, JSONIOException, MBBCommunicationException, ProxyMessageProcessingException {
    JSONObject msgResponse = new JSONObject();
    logger.info("Processing Cancel Message");
    JSONObject messageSent = new JSONObject(message);
    msgResponse.put("serialNo", messageSent.getString("serialNo"));
    msgResponse.put("type", messageSent.getString("type"));
    TVSSignature submitSig = new TVSSignature(SignatureType.BLS,sigKey);
   
    submitSig.update(messageSent.getString("serialNo"));
    System.out.println("Message:" + messageSent.toString());
    if (messageSent.getString("type").equalsIgnoreCase("cancel")) {
      submitSig.update(messageSent.getString("type"));
    }
    else {
      logger.info("Invalid message type received for CancelProxyServlet");
    }

    messageSent.put("cancelAuthID", "CancelAuth");
    messageSent.put("cancelAuthSig", submitSig.signAndEncode(EncodingType.BASE64));
    return messageSent;

  }


}
