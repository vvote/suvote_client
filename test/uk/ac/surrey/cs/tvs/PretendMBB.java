/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants;
import uk.ac.surrey.cs.tvs.fields.messages.JSONConstants;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.FileMessage;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.fields.messages.MessageTypes;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;

/**
 * Class which pretends to be an MBB by listening on a port.
 */
public class PretendMBB extends Thread {

  /**
   * Thread to process a message and a file.
   */
  private class DataThread extends Thread {

    /** The client socket. */
    private Socket     socket  = null;

    /** The message that has been read. */
    private JSONObject message = null;

    /** The file data that has been read. */
    private byte[]     data    = null;

    /** Are we expecting a file? */
    private boolean    file    = true;

    /**
     * @param socket
     *          The client socket.
     * @param response
     *          The response to send.
     * @param file
     *          Do we expect a file as well?
     */
    public DataThread(Socket socket, boolean file) {
      super();

      this.socket = socket;
      this.file = file;
    }

    /**
     * @return the data.
     */
    private byte[] getData() {
      return this.data;
    }

    /**
     * @return the message.
     */
    private JSONObject getMessage() {
      return this.message;
    }

    /**
     * @return The required response.
     */
    private String getResponse() {
      String result = null;

      try {
        String type = this.message.getString(JSONWBBMessage.TYPE);

        String data = "";

        if (type.equals(MessageTypes.AUDIT)) {
          data += type;
          data += this.message.getString(MessageFields.JSONWBBMessage.ID);

          // This matches TestDeviceOne:12 permutations for the audit.
          data += "6,3,2,0,7,8,5,4,1:2,4,0,5,1,6,7,3:2,8,37,4,12,15,13,38,25,34,10,1,22,5,6,35,9,18,17,40,39,16,21,32,29,11,33,20,31,0,28,27,7,23,36,24,26,14,3,19,41,30:";
          //"1,6,7,0,5,8,2,4,3:0,4,1,2,5,6,7,3:0,11,32,19,36,23,31,4,1,10,8,13,14,25,3,30,17,22,39,40,26,2,41,20,9,34,12,37,29,18,35,27,38,5,15,7,6,21,24,16,28,33:";
                  //"3,7,1,4,5,0,2,8,6:3,1,0,4,2,5,7,6:18,13,1,27,19,33,29,14,38,7,34,25,31,41,22,4,3,12,24,23,36,40,11,8,39,5,9,32,16,2,15,21,30,17,28,35,0,20,26,6,37,10:";
        }
        else if (type.equals(MessageTypes.CANCEL)) {
          data += type;
          data += this.message.getString(MessageFields.CancelMessage.ID);
        }
        else if (type.equals("vote")) {
          data += this.message.getString(JSONWBBMessage.ID);
          data += this.message.getString(JSONWBBMessage.DISTRICT);

          // This matches the vote preferences.
          data += "1, , :1, , : , , :";
          data += TestParameters.COMMIT_TIME;
        }
        else if (type.equals("startevm")) {
          data += type;
          data += this.message.getString(JSONWBBMessage.ID);
          data += this.message.getString(JSONWBBMessage.DISTRICT);
        }
        else if (type.equals("pod")) {
          data += this.message.getString(JSONWBBMessage.ID);
          data += this.message.getString(JSONWBBMessage.DISTRICT);
        }
        else {
          data += this.message.getString(FileMessage.ID);
          data += this.message.getString(FileMessage.DIGEST);
          data += this.message.getString(FileMessage.SENDER_ID);
          data += TestParameters.COMMIT_TIME;
        }

        String signature = TestParameters.signData(data,id);

        JSONObject response = new JSONObject();
        response.put(JSONWBBMessage.TYPE, type);
        response.put(ClientConstants.BallotGenCommitResponse.WBB_SIG, signature);
        response.put(JSONConstants.Signature.SIGNER_ID, TestParameters.PEER_ID[id]);
        response.put(MessageFields.PeerResponse.COMMIT_TIME, TestParameters.COMMIT_TIME);

        result = response.toString();
      }
      catch (TVSSignatureException e) {
        e.printStackTrace();
      }
      catch (CryptoIOException e) {
        e.printStackTrace();
      }
      catch (JSONException e) {
        e.printStackTrace();
      }

      return result;
    }

    /**
     * Processes the data for a pretend MBB.
     * 
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {
      ByteArrayOutputStream buffer = new ByteArrayOutputStream();
      BufferedInputStream in = null;
      BufferedWriter out = null;

      try {
        // Suck all the data and save it away.
        in = new BufferedInputStream(this.socket.getInputStream());
        boolean readMessage = false;
        boolean finished = false;
        int readLength = 0;
        int count = 0;
        int b = in.read();

        while ((b != -1) && !finished) {
          count++;

          // We expect a message.
          if (!readMessage && (b == '\n')) {
            this.message = new JSONObject(buffer.toString());

            // See if we expect a file/
            if (this.file) {
              readLength = (Integer) this.message.get("fileSize");
            }

            readMessage = true;
            buffer.reset();
          }

          // And maybe a file.
          if (readMessage && count >= readLength) {
            finished = true;
          }

          buffer.write(b);

          // Only read in the required amount of data.
          if (!finished) {
            b = in.read();
          }
        }

        this.data = buffer.toByteArray();

        // Send the response.
        out = new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream()));
        out.write(this.getResponse());
        out.newLine();
        out.flush();
      }
      catch (IOException e){
        e.printStackTrace();
      }catch( JSONException e) {
        e.printStackTrace();
      }
      finally {
        if (out != null) {
          try {
            out.close();
          }
          catch (IOException e) {
            e.printStackTrace();
          }
        }
        if (in != null) {
          try {
            in.close();
          }
          catch (IOException e) {
            e.printStackTrace();
          }
        }

        try {
          this.socket.close();
        }
        catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }

  /** The port to listen on. */
  private int              port         = 0;

  /** The listening server socket. */
  private ServerSocket     serverSocket = null;

  /** The data threads. */
  private List<DataThread> threads      = new ArrayList<DataThread>();

  /** Are we running? */
  private boolean          running      = true;

  /** Are we expecting a file? */
  private boolean          file         = true;

  /** which peer are we*/
  public int id=0;
  /**
   * Constructor for pretend MBB. Private so that the MBB can only be started and stopped via the static methods.
   * 
   * @param port
   *          The port to listen on.
   */
  private PretendMBB(int port,int id) {
    super();
    this.id=id;
    this.port = port;
  }

  /**
   * @return All of the sub-thread data.
   */
  public List<byte[]> getData() {
    List<byte[]> data = new ArrayList<byte[]>();

    for (DataThread thread : this.threads) {
      data.add(thread.getData());
    }

    return data;
  }

  /**
   * @return All of the sub-thread messages.
   */
  public List<JSONObject> getMessages() {
    List<JSONObject> messages = new ArrayList<JSONObject>();

    for (DataThread thread : this.threads) {
      messages.add(thread.getMessage());
    }

    return messages;
  }

  /**
   * @return the whether the MBB expects a file as well as a message.
   */
  public boolean isFile() {
    return this.file;
  }

  /**
   * Pretends to be the MBB by listening.
   * 
   * @see java.lang.Thread#run()
   */
  @Override
  public void run() {
    // Listen on the specified port and wait for a connection.
    try {
      this.serverSocket = new ServerSocket(this.port);

      while (this.running) {
        DataThread thread = new DataThread(this.serverSocket.accept(), this.file);
        thread.start();
        this.threads.add(thread);
      }
    }
    catch (IOException e) {
      // Ignore - we are expecting a socket error on close.
    }
    finally {
      if (this.serverSocket != null) {
        try {
          this.serverSocket.close();
        }
        catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }

  /**
   * @param file
   *          set whether the MBB expects a file as well as a message.
   */
  public void setFile(boolean file) {
    this.file = file;
  }

  /**
   * Shuts down listening.
   */
  public void shutdown() {
    this.running = false;
    try {
      this.serverSocket.close();
    }
    catch (IOException e1) {
      e1.printStackTrace();
    }

    // Wait for the sub-threads.
    for (Thread thread : this.threads) {
      try {
        thread.join();
      }
      catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * Starts up a pretend MBB as a series of peers on separate threads.
   * 
   * @return The pretend MBB threads.
   * @throws JSONException
   */
  public static List<PretendMBB> startPrentendMBB() throws JSONException {
    return startPrentendMBB(true);
  }

  /**
   * Starts up a pretend MBB as a series of peers on separate threads with optional file parameter.
   * 
   * @param file
   *          Do we expect a file as well?
   * @return The pretend MBB threads.
   * @throws JSONException
   */
  public static List<PretendMBB> startPrentendMBB(boolean file) throws JSONException {
    // Run some pretend MBBs to receive the data.
    int[] ports = new int[] { 9091, 9092, 9093 };
    List<PretendMBB> pretendMBBs = new ArrayList<PretendMBB>();

    for (int i = 0; i < ports.length; i++) {
      PretendMBB pretendMBB = new PretendMBB(ports[i],i);
      pretendMBB.setFile(file);
      pretendMBB.start();
      pretendMBBs.add(pretendMBB);
    }

    return pretendMBBs;
  }

  /**
   * Shuts down and waits for the pretend MBB.
   * 
   * @param pretendMBBs
   *          The list of peer threads.
   * @throws InterruptedException
   */
  public static void stopPretendMBB(List<PretendMBB> pretendMBBs) throws InterruptedException {
    for (PretendMBB pretendMBB : pretendMBBs) {
      pretendMBB.shutdown();
      pretendMBB.join();
    }
  }
}
