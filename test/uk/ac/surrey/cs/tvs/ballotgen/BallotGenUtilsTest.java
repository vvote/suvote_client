/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigInteger;
import java.security.MessageDigest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.TestParameters;
import uk.ac.surrey.cs.tvs.ballotgen.init.CandidateIDs;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.FileMessage;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.io.ConfigFile;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * The class <code>BallotGenUtilsTest</code> contains tests for the class <code>{@link BallotGenUtils}</code>.
 */
public class BallotGenUtilsTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();

    // Copy over a clean version of the configuration file.
    TestParameters.copyFile(new File(TestParameters.CLEAN_CLIENT_CONFIG_FILE), new File(TestParameters.OUTPUT_CLIENT_CONFIG_FILE));

    // Generate candidate ids.
    CandidateIDs.create(TestParameters.CANDIDATES, TestParameters.PUBLIC_KEY_FILE, TestParameters.OUTPUT_PLAIN_TEXT_FILE,
        TestParameters.OUTPUT_ENCRYPTED_FILE);
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the BallotGenUtils() constructor test.
   */
  @Test
  public void testBallotGenUtils_1() throws Exception {
    BallotGenUtils result = new BallotGenUtils();
    assertNotNull(result);
  }

  /**
   * Run the void createAuditFile(byte[],String,int,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCreateAuditFile_1() throws Exception {
    BallotGenMix generation = new BallotGenMix(new ConfigFile(TestParameters.OUTPUT_CLIENT_CONFIG_FILE));

    // Generate the ballots.
    generation.generateBallots(TestParameters.OUTPUT_BALLOT_FOLDER, TestParameters.OUTPUT_BALLOT_CIPHERS_FILE);

    // Test creation of the audit file.
    //TODO CJC: I changed this because the seed should be the output of the hash and thus 256 bits in size
    MessageDigest md=MessageDigest.getInstance("SHA256");
    md.update(BigInteger.TEN.toByteArray());
    BallotGenUtils.createAuditFile(md.digest(), TestParameters.OUTPUT_BALLOT_CIPHERS_FILE, TestParameters.BALLOTS,
        TestParameters.AUDIT_FILE, TestParameters.DEVICE_NAME);

    // Test that the file has been created with the correct number of ballots to audit.
    BufferedReader inReader = null;

    try {
      inReader = new BufferedReader(new FileReader(TestParameters.AUDIT_FILE));

      // Single object.
      String inLine = inReader.readLine();
      JSONObject auditObject = new JSONObject(inLine);
      assertTrue(auditObject.has("auditSerials"));

      JSONArray serials = auditObject.getJSONArray("auditSerials");
      assertEquals(TestParameters.BALLOTS, serials.length());

      for (int i = 0; i < serials.length(); i++) {
        assertTrue(serials.get(i).toString().startsWith(TestParameters.DEVICE_NAME));
      }
    }
    // Pass all exceptions up.
    finally {
      if (inReader != null) {
        inReader.close();
      }
    }
  }

  /**
   * Run the void createAuditFile(byte[],String,int,String) method test.
   * 
   * @throws Exception
   */
  @Test(expected = uk.ac.surrey.cs.tvs.ballotgen.exceptions.AuditFileCreationException.class)
  public void testCreateAuditFile_2() throws Exception {
    BallotGenMix generation = new BallotGenMix(new ConfigFile(TestParameters.OUTPUT_CLIENT_CONFIG_FILE));

    // Generate the ballots.
    generation.generateBallots(TestParameters.OUTPUT_BALLOT_FOLDER, TestParameters.OUTPUT_BALLOT_CIPHERS_FILE);

    // Test creation of the audit file.
    BallotGenUtils.createAuditFile(BigInteger.TEN.toByteArray(), TestParameters.OUTPUT_BALLOT_CIPHERS_FILE,
        TestParameters.BALLOTS + 1, TestParameters.AUDIT_FILE, TestParameters.DEVICE_NAME);
  }

  /**
   * Run the void createInputCiphers(int,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCreateInputCiphers_1() throws Exception {
    BallotGenUtils.createInputCiphers(TestParameters.BALLOTS, TestParameters.OUTPUT_ENCRYPTED_FILE,
        TestParameters.OUTPUT_BALLOT_CIPHERS_PRINT_FILE);

    // Test that the output file contains the required number of duplicate entries.
    BufferedReader inReader = null;
    BufferedReader outReader = null;

    try {
      inReader = new BufferedReader(new FileReader(TestParameters.OUTPUT_ENCRYPTED_FILE));
      outReader = new BufferedReader(new FileReader(TestParameters.OUTPUT_BALLOT_CIPHERS_PRINT_FILE));

      // Single input object.
      String inLine = inReader.readLine();

      // Duplicated output file.
      int count = 0;
      String outLine = outReader.readLine();

      while (outLine != null) {
        count++;
        assertEquals(inLine, outLine);
        outLine = outReader.readLine();
      }

      assertEquals(TestParameters.BALLOTS, count);
    }
    // Pass all exceptions up.
    finally {
      if (inReader != null) {
        inReader.close();
      }
      if (outReader != null) {
        outReader.close();
      }
    }
  }

  /**
   * Run the JSONObject createMBBMessage(BallotGenConf,String,MessageDigest,long) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCreateMBBMessage_1() throws Exception {
    MessageDigest fileDigest = MessageDigest.getInstance("SHA1");
    ConfigFile config = new ConfigFile(TestParameters.OUTPUT_CLIENT_CONFIG_FILE);
    
    JSONObject result = BallotGenUtils.createMBBMessage(config, "anything", fileDigest, 0l);
    assertNotNull(result);

    assertTrue(result.has(JSONWBBMessage.TYPE));
    assertTrue(result.has(FileMessage.SENDER_ID));
    assertTrue(result.has(FileMessage.ID));
    assertTrue(result.has(FileMessage.SENDER_SIG));
    assertTrue(result.has(FileMessage.DIGEST));
    assertTrue(result.has(FileMessage.FILESIZE));

    assertEquals("anything", result.get(JSONWBBMessage.TYPE));
    assertTrue(result.get(FileMessage.SENDER_ID).toString().length() > 0);
    assertTrue(result.get(FileMessage.ID).toString().length() > 0);
    assertEquals(TestParameters.DEVICE_NAME, result.get(FileMessage.SENDER_ID));
    assertEquals(IOUtils.encodeData(EncodingType.BASE64, fileDigest.digest()), result.get(FileMessage.DIGEST));
    assertEquals(0l, result.get(FileMessage.FILESIZE));
  }
}