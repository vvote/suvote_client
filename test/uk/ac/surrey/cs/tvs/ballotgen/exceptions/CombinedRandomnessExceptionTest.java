/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.TestParameters;

/**
 * The class <code>CombinedRandomnessExceptionTest</code> contains tests for the class
 * <code>{@link CombinedRandomnessException}</code>.
 */
public class CombinedRandomnessExceptionTest {

  /**
   * Run the CombinedRandomnessException() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCombinedRandomnessException_1() throws Exception {

    CombinedRandomnessException result = new CombinedRandomnessException();

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.CombinedRandomnessException", result.toString());
    assertEquals(null, result.getMessage());
    assertEquals(null, result.getLocalizedMessage());
  }

  /**
   * Run the CombinedRandomnessException(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCombinedRandomnessException_2() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;

    CombinedRandomnessException result = new CombinedRandomnessException(message);

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.CombinedRandomnessException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the CombinedRandomnessException(Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCombinedRandomnessException_3() throws Exception {
    Throwable cause = new Throwable();

    CombinedRandomnessException result = new CombinedRandomnessException(cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.CombinedRandomnessException: java.lang.Throwable", result.toString());
    assertEquals("java.lang.Throwable", result.getMessage());
    assertEquals("java.lang.Throwable", result.getLocalizedMessage());
  }

  /**
   * Run the CombinedRandomnessException(String,Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCombinedRandomnessException_4() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();

    CombinedRandomnessException result = new CombinedRandomnessException(message, cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.CombinedRandomnessException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the CombinedRandomnessException(String,Throwable,boolean,boolean) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCombinedRandomnessException_5() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();
    boolean enableSuppression = true;
    boolean writableStackTrace = true;

    CombinedRandomnessException result = new CombinedRandomnessException(message, cause, enableSuppression, writableStackTrace);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.CombinedRandomnessException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }
}