/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.TestParameters;

/**
 * The class <code>RandomnessCommitmentExceptionTest</code> contains tests for the class
 * <code>{@link RandomnessCommitmentException}</code>.
 */
public class RandomnessCommitmentExceptionTest {

  /**
   * Run the RandomnessCommitmentException() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testRandomnessCommitmentException_1() throws Exception {

    RandomnessCommitmentException result = new RandomnessCommitmentException();

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.RandomnessCommitmentException", result.toString());
    assertEquals(null, result.getMessage());
    assertEquals(null, result.getLocalizedMessage());
  }

  /**
   * Run the RandomnessCommitmentException(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testRandomnessCommitmentException_2() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;

    RandomnessCommitmentException result = new RandomnessCommitmentException(message);

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.RandomnessCommitmentException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the RandomnessCommitmentException(Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testRandomnessCommitmentException_3() throws Exception {
    Throwable cause = new Throwable();

    RandomnessCommitmentException result = new RandomnessCommitmentException(cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.RandomnessCommitmentException: java.lang.Throwable", result.toString());
    assertEquals("java.lang.Throwable", result.getMessage());
    assertEquals("java.lang.Throwable", result.getLocalizedMessage());
  }

  /**
   * Run the RandomnessCommitmentException(String,Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testRandomnessCommitmentException_4() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();

    RandomnessCommitmentException result = new RandomnessCommitmentException(message, cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.RandomnessCommitmentException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the RandomnessCommitmentException(String,Throwable,boolean,boolean) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testRandomnessCommitmentException_5() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();
    boolean enableSuppression = true;
    boolean writableStackTrace = true;

    RandomnessCommitmentException result = new RandomnessCommitmentException(message, cause, enableSuppression, writableStackTrace);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.RandomnessCommitmentException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }
}