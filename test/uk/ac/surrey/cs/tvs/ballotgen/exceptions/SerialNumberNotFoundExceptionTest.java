/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.TestParameters;

/**
 * The class <code>SerialNumberNotFoundExceptionTest</code> contains tests for the class
 * <code>{@link SerialNumberNotFoundException}</code>.
 */
public class SerialNumberNotFoundExceptionTest {

  /**
   * Run the SerialNumberNotFoundException() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testSerialNumberNotFoundException_1() throws Exception {

    SerialNumberNotFoundException result = new SerialNumberNotFoundException();

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.SerialNumberNotFoundException", result.toString());
    assertEquals(null, result.getMessage());
    assertEquals(null, result.getLocalizedMessage());
  }

  /**
   * Run the SerialNumberNotFoundException(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testSerialNumberNotFoundException_2() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;

    SerialNumberNotFoundException result = new SerialNumberNotFoundException(message);

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.SerialNumberNotFoundException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the SerialNumberNotFoundException(Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testSerialNumberNotFoundException_3() throws Exception {
    Throwable cause = new Throwable();

    SerialNumberNotFoundException result = new SerialNumberNotFoundException(cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.SerialNumberNotFoundException: java.lang.Throwable", result.toString());
    assertEquals("java.lang.Throwable", result.getMessage());
    assertEquals("java.lang.Throwable", result.getLocalizedMessage());
  }

  /**
   * Run the SerialNumberNotFoundException(String,Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testSerialNumberNotFoundException_4() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();

    SerialNumberNotFoundException result = new SerialNumberNotFoundException(message, cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.SerialNumberNotFoundException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the SerialNumberNotFoundException(String,Throwable,boolean,boolean) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testSerialNumberNotFoundException_5() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();
    boolean enableSuppression = true;
    boolean writableStackTrace = true;

    SerialNumberNotFoundException result = new SerialNumberNotFoundException(message, cause, enableSuppression, writableStackTrace);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.SerialNumberNotFoundException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }
}