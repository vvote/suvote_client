/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.TestParameters;

/**
 * The class <code>BallotGenConfTest</code> contains tests for the class <code>{@link BallotGenConf}</code>.
 */
public class BallotGenConfTest {

  /**
   * Run the BallotGenConf() constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException.class)
  public void testBallotGenConf_1() throws Exception {
    BallotGenConf result = new BallotGenConf();
    assertNull(result);
  }

  /**
   * Run the BallotGenConf(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException.class)
  public void testBallotGenConf_2() throws Exception {
    String configFile = "no such file found";

    BallotGenConf result = new BallotGenConf(configFile);
    assertNull(result);
  }

  /**
   * Run the BallotGenConf(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testBallotGenConf_3() throws Exception {
    BallotGenConf result = new BallotGenConf(TestParameters.BALLOT_GENERATION_CONFIG_FILE);
    assertNotNull(result);
  }

  /**
   * Run the int[] getRaceCounts() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetRaceCounts_1() throws Exception {
    BallotGenConf fixture = new BallotGenConf(TestParameters.BALLOT_GENERATION_CONFIG_FILE);
    assertNotNull(fixture);

    int[] result = fixture.getRaceCounts();
    assertNotNull(result);

    // Test that the correct number of candidates and races have been read in.
    assertEquals(TestParameters.RACE_CANDIDATES.length, result.length);

    for (int i = 0; i < result.length; i++) {
      assertEquals(TestParameters.RACE_CANDIDATES[i], result[i]);
    }
  }
}