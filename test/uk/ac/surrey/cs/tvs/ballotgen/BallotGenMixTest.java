/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.PretendMBB;
import uk.ac.surrey.cs.tvs.TestParameters;
import uk.ac.surrey.cs.tvs.ballotgen.exceptions.ClientException;
import uk.ac.surrey.cs.tvs.ballotgen.init.CandidateIDs;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.FileMessage;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.fields.messages.MessageTypes;
import uk.ac.surrey.cs.tvs.ui.ProgressListener;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.io.ConfigFile;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * The class <code>BallotGenMixTest</code> contains tests for the class <code>{@link BallotGenMix}</code>.
 */
public class BallotGenMixTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();

    // Copy over a clean version of the configuration file.
    TestParameters.copyFile(new File(TestParameters.CLEAN_CLIENT_CONFIG_FILE), new File(TestParameters.OUTPUT_CLIENT_CONFIG_FILE));

    // Generate candidate ids.
    CandidateIDs.create(TestParameters.CANDIDATES, TestParameters.PUBLIC_KEY_FILE, TestParameters.OUTPUT_PLAIN_TEXT_FILE,
        TestParameters.OUTPUT_ENCRYPTED_FILE);
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Tests the generated ballot files and their content.
   * 
   * @throws IOException
   * @throws JSONException
   */
  private void testBallotFiles() throws IOException, JSONException {
    // ballotDB: single entry for next ballot.
    BufferedReader inReader = null;

    try {
      inReader = new BufferedReader(new FileReader(TestParameters.OUTPUT_BALLOT_DB_FILE));

      // Single object.
      String inLine = inReader.readLine();
      JSONObject ballotDBObject = new JSONObject(inLine);

      assertTrue(ballotDBObject.has("nextBallot"));
      assertEquals(0, ballotDBObject.get("nextBallot"));
    }
    // Pass all exceptions up.
    finally {
      if (inReader != null) {
        inReader.close();
      }
    }

    // Output folder: one file per ballot with content.
    File ballotsFolder = new File(TestParameters.OUTPUT_BALLOT_FOLDER);
    File[] ballotFiles = ballotsFolder.listFiles();
    Arrays.sort(ballotFiles);

    // We should have the correct number of ballots.
    assertEquals(TestParameters.BALLOTS, ballotFiles.length);

    // Each file should contain data matching its filename.
    for (File ballotFile : ballotFiles) {
      try {
        inReader = new BufferedReader(new FileReader(ballotFile));

        // Single object.
        String inLine = inReader.readLine();
        JSONObject ballotObject = new JSONObject(inLine);

        assertTrue(ballotObject.has("serialNo"));
        assertEquals(ballotFile.getName(), ((String) ballotObject.get("serialNo")).replaceAll("\\W", "_") + ".json");

        assertTrue(ballotObject.has("permutations"));
        assertTrue(ballotObject.has("combinedRandomness"));

        JSONArray permutations = ballotObject.getJSONArray("permutations");
        assertEquals(TestParameters.RACE_CANDIDATES.length, permutations.length());

        JSONArray randomness = ballotObject.getJSONArray("combinedRandomness");
        assertEquals(TestParameters.RACE_CANDIDATES.length, randomness.length());

        for (int i = 0; i < TestParameters.RACE_CANDIDATES.length; i++) {
          assertEquals(TestParameters.RACE_CANDIDATES[i], permutations.getJSONArray(i).length());
          assertEquals(TestParameters.RACE_CANDIDATES[i], randomness.getJSONArray(i).length());
        }

        assertTrue(ballotObject.has("commitWitness"));
      }
      // Pass all exceptions up.
      finally {
        if (inReader != null) {
          inReader.close();
        }
      }
    }

    // ballots.json: JSON array of ballot file names matching files in output folder.
    try {
      inReader = new BufferedReader(new FileReader(TestParameters.OUTPUT_BALLOT_LIST_FILE));

      // Single object.
      String inLine = inReader.readLine();
      JSONArray ballotList = new JSONArray(inLine);

      assertEquals(TestParameters.BALLOTS, ballotList.length());

      // Extract the list of files and sort them.
      String[] fileNames = new String[TestParameters.BALLOTS];

      for (int i = 0; i < ballotList.length(); i++) {
        fileNames[i] = ballotList.getString(i);
      }
      Arrays.sort(fileNames);

      // Test that the names match.
      for (int i = 0; i < fileNames.length; i++) {
        assertEquals(TestParameters.OUTPUT_BALLOT_FOLDER + File.separator + ballotFiles[i].getName(), fileNames[i]);
      }
    }
    // Pass all exceptions up.
    finally {
      if (inReader != null) {
        inReader.close();
      }
    }

    // wbbciphers.json: one entry per ballot with content.
    int totalCiphers = 0;

    for (int i = 0; i < TestParameters.RACE_CANDIDATES.length; i++) {
      totalCiphers += TestParameters.RACE_CANDIDATES[i];
    }

    try {
      inReader = new BufferedReader(new FileReader(TestParameters.OUTPUT_BALLOT_CIPHERS_FILE));
      int count = 0;
      String inLine = inReader.readLine();

      while (inLine != null) {
        count++;

        // Convert the line into a JSON object.
        JSONObject ballotCipher = new JSONObject(inLine);

        // Check the content.
        assertTrue(ballotCipher.has("serialNo"));
        assertEquals(TestParameters.DEVICE_NAME + ":" + count, ballotCipher.get("serialNo"));

        assertTrue(ballotCipher.has("ciphers"));
        assertEquals(totalCiphers, ballotCipher.getJSONArray("ciphers").length());

        assertTrue(ballotCipher.has("permutation"));

        inLine = inReader.readLine();
      }

      assertEquals(TestParameters.BALLOTS, count);
    }
    // Pass all exceptions up.
    finally {
      if (inReader != null) {
        inReader.close();
      }
    }
  }

  /**
   * Run the BallotGenMix(String,String,String,String,String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testBallotGenMix_1() throws Exception {
    BallotGenMix result = new BallotGenMix(new ConfigFile(TestParameters.OUTPUT_CLIENT_CONFIG_FILE));
    assertNotNull(result);
  }

  /**
   * Run the BallotGenMix(String,String,String,String,String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = ClientException.class)
  public void testBallotGenMix_2() throws Exception {
    // Modify the configuration to point to an empty randomness folder.
    ConfigFile configFile = new ConfigFile(TestParameters.OUTPUT_CLIENT_CONFIG_FILE);
    configFile.setStringParameter(ConfigFiles.ClientConfig.RANDOMNESS_FOLDER, "rubbish");

    BallotGenMix result = new BallotGenMix(configFile);
    assertNull(result);
  }

  /**
   * Run the void generateBallots(String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGenerateBallots_1() throws Exception {
    BallotGenMix fixture = new BallotGenMix(new ConfigFile(TestParameters.OUTPUT_CLIENT_CONFIG_FILE));

    // Generate the ballots.
    fixture.generateBallots(TestParameters.OUTPUT_BALLOT_FOLDER, TestParameters.OUTPUT_BALLOT_CIPHERS_FILE);

    // Test the content of the generated files.
    this.testBallotFiles();
  }

  /**
   * Run the void performAudit(JSONObject,String,String,String,String,String,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPerformAudit_1() throws Exception {
    System.out.println(IOUtils.readJSONObjectFromFile(TestParameters.OUTPUT_CLIENT_CONFIG_FILE).toString());
    BallotGenMix fixture = new BallotGenMix(new ConfigFile(TestParameters.OUTPUT_CLIENT_CONFIG_FILE));

    // Generate ballots.
    fixture.generateBallots(TestParameters.OUTPUT_BALLOT_FOLDER, TestParameters.OUTPUT_BALLOT_CIPHERS_FILE);

    // Startup a pretend MBB.
    List<PretendMBB> pretendMBBs = PretendMBB.startPrentendMBB();

  //TODO CJC: I changed this because the seed should be the output of the hash and thus 256 bits in size
    MessageDigest md=MessageDigest.getInstance("SHA256");
    md.update(BigInteger.TEN.toByteArray());
    // Test auditing of the ballots.
    JSONObject generation = new JSONObject();
    generation.put(ClientConstants.BallotGenCommitResponse.FIAT_SHAMIR,
        IOUtils.encodeData(EncodingType.BASE64,md.digest()));

    ProgressListener listener = new ProgressListener() {

      @Override
      public void updateProgress(int value) {
        assertTrue(value >= 0);
      }
    };

    fixture.addProgressListener(listener);
    assertFalse(new File(TestParameters.AUDIT_FILE).exists());
    fixture.performAudit(generation, TestParameters.OUTPUT_BALLOT_CIPHERS_FILE, TestParameters.AUDIT_FILE,
        TestParameters.OUTPUT_AUDIT_FILE, TestParameters.DEVICE_KEY_PAIR_FILE, TestParameters.RANDOM_FOLDER,
        TestParameters.OUTPUT_AUDIT_RESPONSE_FILE, TestParameters.OUTPUT_AUDIT_MBB_ZIP_FILE);

    fixture.removeProgressListener(listener);

    // Shutdown the MBB.
    PretendMBB.stopPretendMBB(pretendMBBs);

    // Test that the audit serials number file was created. We do not test the content as this is tested in the BallotGenUtilsTest
    // class.
    assertTrue(new File(TestParameters.AUDIT_FILE).exists());

    // We do not need to check the response from the MBB as this is tested in the BallotGenAuditTest class. Just test the response.
    BufferedReader inReader = null;

    try {
      inReader = new BufferedReader(new FileReader(TestParameters.OUTPUT_AUDIT_RESPONSE_FILE));

      // Single object.
      String inLine = inReader.readLine();
      JSONObject auditResponse = new JSONObject(inLine);

      assertTrue(auditResponse.has(JSONWBBMessage.TYPE));
      assertTrue(auditResponse.has(FileMessage.SENDER_ID));
      assertTrue(auditResponse.has(ClientConstants.BallotGenCommitResponse.SUBMISSION_ID));
      assertTrue(auditResponse.has(FileMessage.SENDER_SIG));
      assertTrue(auditResponse.has(FileMessage.DIGEST));
      assertTrue(auditResponse.has(FileMessage.FILESIZE));
      assertTrue(auditResponse.has(ClientConstants.BallotGenCommitResponse.WBB_SIG));

      assertEquals(MessageTypes.BALLOT_AUDIT_COMMIT, auditResponse.get(JSONWBBMessage.TYPE));
      assertEquals(TestParameters.DEVICE_NAME, auditResponse.get(FileMessage.SENDER_ID));
    }
    // Pass all exceptions up.
    finally {
      if (inReader != null) {
        inReader.close();
      }
    }
  }

  /**
   * Run the JSONObject submitBallotsToWBB(String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSubmitBallotsToWBB_1() throws Exception {
    BallotGenMix fixture = new BallotGenMix(new ConfigFile(TestParameters.OUTPUT_CLIENT_CONFIG_FILE));

    // Generate ballots.
    fixture.generateBallots(TestParameters.OUTPUT_BALLOT_FOLDER, TestParameters.OUTPUT_BALLOT_CIPHERS_FILE);

    // Startup a pretend MBB.
    List<PretendMBB> pretendMBBs = PretendMBB.startPrentendMBB();

    // Test submission of the ballot ciphers.
    JSONObject result = fixture.submitBallotsToWBB(TestParameters.OUTPUT_BALLOT_CIPHERS_FILE,
        TestParameters.OUTPUT_BALLOT_MBB_ZIP_FILE);
    assertNotNull(result);

    assertTrue(result.has(ClientConstants.BallotGenCommitResponse.PEER_ID));
    assertEquals(TestParameters.DEVICE_NAME, result.get(ClientConstants.BallotGenCommitResponse.PEER_ID));

    assertTrue(result.has(ClientConstants.BallotGenCommitResponse.SUBMISSION_ID));

    assertTrue(result.has(ClientConstants.BallotGenCommitResponse.BALLOT_FILE));
    assertEquals(TestParameters.OUTPUT_BALLOT_CIPHERS_FILE, result.get(ClientConstants.BallotGenCommitResponse.BALLOT_FILE));

    assertTrue(result.has(ClientConstants.BallotGenCommitResponse.WBB_SIG));
    assertTrue(result.has(ClientConstants.BallotGenCommitResponse.FIAT_SHAMIR));

    // Shutdown the MBB.
    PretendMBB.stopPretendMBB(pretendMBBs);

    // Check what the MBB received.
    for (PretendMBB pretendMBB : pretendMBBs) {
      // Test that the correct messages were sent. Only some content is tested.
      List<JSONObject> messages = pretendMBB.getMessages();
      assertEquals(1, messages.size());

      for (JSONObject message : messages) {
        assertTrue(message.has(JSONWBBMessage.TYPE));
        assertTrue(message.has(FileMessage.SENDER_ID));
        assertTrue(message.has(ClientConstants.BallotGenCommitResponse.SUBMISSION_ID));
        assertTrue(message.has(FileMessage.SENDER_SIG));
        assertTrue(message.has(FileMessage.DIGEST));
        assertTrue(message.has(FileMessage.FILESIZE));

        assertEquals(MessageTypes.BALLOT_GEN_COMMIT, message.get(JSONWBBMessage.TYPE));
        assertEquals(TestParameters.DEVICE_NAME, message.get(FileMessage.SENDER_ID));
      }

      // And that the data was also sent. Content is not tested.
      List<byte[]> data = pretendMBB.getData();
      assertEquals(1, data.size());
    }

    // Test that the content of the output ZIP file is the same as the ballot output file.
    File zipFolder = new File(TestParameters.OUTPUT_ZIP_FOLDER);
    TestParameters.extractZip(new File(TestParameters.OUTPUT_BALLOT_MBB_ZIP_FILE), zipFolder);
    File[] zipFiles = zipFolder.listFiles();
    Arrays.sort(zipFiles);

    File zipBallotFile = new File(zipFolder, TestParameters.OUTPUT_BALLOT_CIPHERS_FILE);

    assertEquals(1, zipFiles.length);
    assertEquals(zipBallotFile, zipFiles[0]);

    assertTrue(TestParameters.compareFile(new File(TestParameters.OUTPUT_BALLOT_CIPHERS_FILE), zipBallotFile));
  }

  /**
   * Run the JSONObject submitBallotsToWBB(String,String) method test.
   * 
   * @throws Exception
   */
  @Test(expected = uk.ac.surrey.cs.tvs.comms.exceptions.MBBCommunicationException.class)
  public void testSubmitBallotsToWBB_2() throws Exception {
    BallotGenMix fixture = new BallotGenMix(new ConfigFile(TestParameters.OUTPUT_CLIENT_CONFIG_FILE));

    // Generate ballots.
    fixture.generateBallots(TestParameters.OUTPUT_BALLOT_FOLDER, TestParameters.OUTPUT_BALLOT_CIPHERS_FILE);

    // Test submission of the ballot ciphers.
    JSONObject result = fixture.submitBallotsToWBB(TestParameters.OUTPUT_BALLOT_CIPHERS_FILE,
        TestParameters.OUTPUT_BALLOT_MBB_ZIP_FILE);
    assertNull(result);
  }
}