/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import javax.crypto.Cipher;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.TestParameters;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * The class <code>RandomnessRowTest</code> contains tests for the class <code>{@link RandomnessRow}</code>.
 */
public class RandomnessRowTest {

  /**
   * Run the byte[] getNextValue() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetNextValue_1() throws Exception {
    RandomnessRow fixture = new RandomnessRow(TestParameters.getInstance().getRandomness().toString(), TestParameters.getInstance()
        .getDecryptCipher(), TestParameters.getInstance().getCommitment().toString());

    // Decrypt the commitment independently.
    Cipher cipher = TestParameters.getInstance().getDecryptCipher();
    String randomness = TestParameters.getInstance().getRandomness().getJSONArray("randomness").getJSONObject(0).getString("r");
    byte[] decrypted = cipher.doFinal(IOUtils.decodeData(EncodingType.HEX, randomness));
    byte[] result = fixture.getNextValue();
    assertEquals(decrypted.length, result.length);

    for (int i = 0; i < result.length; i++) {
      assertEquals(decrypted[i], result[i]);
    }
  }

  /**
   * Run the byte[] getNextValue() method test.
   * 
   * @throws Exception
   */
  @Test(expected = uk.ac.surrey.cs.tvs.ballotgen.exceptions.RandomnessCommitmentException.class)
  public void testGetNextValue_2() throws Exception {
    JSONObject wrongCommitment = new JSONObject();
    JSONArray commitments = new JSONArray();
    commitments.put("00");
    wrongCommitment.put("randomness", commitments);

    RandomnessRow fixture = new RandomnessRow(TestParameters.getInstance().getRandomness().toString(), TestParameters.getInstance()
        .getDecryptCipher(), wrongCommitment.toString());

    byte[] result = fixture.getNextValue();
    assertNull(result);
  }

  /**
   * Run the byte[] getNextValue() method test.
   * 
   * @throws Exception
   */
  @Test(expected = uk.ac.surrey.cs.tvs.ballotgen.exceptions.RandomnessCommitmentException.class)
  public void testGetNextValue_3() throws Exception {
    JSONObject wrongCommitment = new JSONObject();
    JSONArray commitments = new JSONArray();
    commitments.put("0");
    wrongCommitment.put("randomness", commitments);

    RandomnessRow fixture = new RandomnessRow(TestParameters.getInstance().getRandomness().toString(), TestParameters.getInstance()
        .getDecryptCipher(), wrongCommitment.toString());

    byte[] result = fixture.getNextValue();
    assertNull(result);
  }

  /**
   * Run the String getSerialNo() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetSerialNo_1() throws Exception {
    RandomnessRow fixture = new RandomnessRow(TestParameters.getInstance().getRandomness().toString(), TestParameters.getInstance()
        .getDecryptCipher(), TestParameters.getInstance().getCommitment().toString());

    assertEquals(TestParameters.getInstance().getRandomness().get("serialNo"), fixture.getSerialNo());
  }

  /**
   * Run the RandomnessRow(String,Cipher,String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testRandomnessRow_1() throws Exception {
    Cipher decrypt = TestParameters.getInstance().getDecryptCipher();
    RandomnessRow result = new RandomnessRow(TestParameters.getInstance().getRandomness().toString(), decrypt, TestParameters
        .getInstance().getCommitment().toString());
    assertNotNull(result);

    // Test the audit has decrypted the data.
    JSONObject auditRow = result.getAuditData();
    JSONArray decryptedRandom = auditRow.getJSONArray(MessageFields.RandomnessFile.RANDOMNESS);

    JSONObject randomRow = TestParameters.getInstance().getRandomness();
    JSONArray unorderedArray = randomRow.getJSONArray(MessageFields.RandomnessFile.RANDOMNESS);
    JSONArray encryptedRandom = new JSONArray();

    for (int i = 0; i < unorderedArray.length(); i++) {
      encryptedRandom.put(unorderedArray.get(i));
    }

    assertEquals(encryptedRandom.length(), decryptedRandom.length());
  }

  /**
   * Run the void setPeerID(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSetPeerID_1() throws Exception {
    RandomnessRow fixture = new RandomnessRow(TestParameters.getInstance().getRandomness().toString(), TestParameters.getInstance()
        .getDecryptCipher(), TestParameters.getInstance().getCommitment().toString());

    assertFalse(fixture.getAuditData().has("peerID"));

    fixture.setPeerID(TestParameters.RANDOMNESS_SOURCE);

    assertTrue(fixture.getAuditData().has("peerID"));
    assertEquals(TestParameters.RANDOMNESS_SOURCE, fixture.getAuditData().get("peerID"));
  }
}