/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * The class <code>AllTests</code> builds a suite that can be used to run all of the tests within its package as well as within any
 * subpackages of its package.
 * 
 * @generatedBy CodePro at 21/08/13 09:57
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ uk.ac.surrey.cs.tvs.ballotgen.AllTests.class, uk.ac.surrey.cs.tvs.client.AllTests.class,
    uk.ac.surrey.cs.tvs.ui.AllTests.class, uk.ac.surrey.cs.tvs.web.AllTests.class, })
public class AllTests {
}
