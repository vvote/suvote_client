package uk.ac.surrey.cs.tvs.ui;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * The class <code>AllTests</code> builds a suite that can be used to run all of the tests within its package as well as within any
 * subpackages of its package.
 * 
 * @generatedBy CodePro at 07/11/13 09:20
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ ClientUITest.class, })
public class AllTests {
}
