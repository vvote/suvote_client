/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.net.ConnectException;
import java.net.Socket;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.TestParameters;
import uk.ac.surrey.cs.tvs.client.ClientType;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles.ClientConfig;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * The class <code>ProxyServerTest</code> contains tests for the class <code>{@link ProxyServer}</code>.
 */
public class ProxyServerTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Copy over a clean version of the configuration file.
    TestParameters.copyFile(new File(TestParameters.CLEAN_CLIENT_CONFIG_FILE), new File(TestParameters.OUTPUT_CLIENT_CONFIG_FILE));
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the JSONObject constructErrorMessage(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testConstructErrorMessage_1() throws Exception {
    JSONObject message = ProxyServer.constructErrorMessage("rubbish");

    assertTrue(message.has(ClientConstants.UIERROR.ERROR));
    assertEquals("rubbish", message.getString(ClientConstants.UIERROR.ERROR));
  }

  /**
   * Run the JSONObject constructErrorMessage(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testConstructErrorMessage_2() throws Exception {
    JSONArray array = new JSONArray();
    JSONObject message = ProxyServer.constructErrorMessage(array, "rubbish");

    assertTrue(message.has(ClientConstants.UIERROR.ERROR));
    assertEquals("rubbish", message.getString(ClientConstants.UIERROR.ERROR));

    assertTrue(message.has(ClientConstants.UIERROR.WBB_RESPONSES));
    assertEquals(array, message.getJSONArray(ClientConstants.UIERROR.WBB_RESPONSES));
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test(expected = ConnectException.class)
  public void testMain_1() throws Exception {
    ProxyServer.main(new String[0]);

    // Attempt to connect to the server.
    Socket socket = null;

    try {
      socket = new Socket("localhost", TestParameters.PROXY_PORTS[0]);
      assertNull(socket.getInetAddress());
    }
    // Pass all exceptions up.
    finally {
      if (socket != null) {
        socket.close();
      }
    }
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_2() throws Exception {
    String input = "stop";
    System.setIn(new ByteArrayInputStream(input.getBytes()));
    ProxyServer.main(new String[] { Integer.toString(TestParameters.PROXY_PORTS[0]), TestParameters.PROXY_ROOT,
        TestParameters.OUTPUT_CLIENT_CONFIG_FILE });

    // Attempt to connect to the server.
    Socket socket = null;

    try {
      socket = new Socket("localhost", TestParameters.PROXY_PORTS[0]);
      assertNotNull(socket.getInetAddress());
    }
    // Pass all exceptions up.
    finally {
      if (socket != null) {
        socket.close();
      }
    }
  }

  /**
   * Run the ProxyServer(String,String,String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testProxyServer_1() throws Exception {
    // Modify the configuration to initialise a cancel client.
    JSONObject config = IOUtils.readJSONObjectFromFile(TestParameters.OUTPUT_CLIENT_CONFIG_FILE);
    config.put(ClientConfig.PURPOSE, ClientType.CANCEL);
    IOUtils.writeJSONToFile(config, TestParameters.OUTPUT_CLIENT_CONFIG_FILE);

    ProxyServer result = new ProxyServer(TestParameters.OUTPUT_CLIENT_CONFIG_FILE, TestParameters.DISTRICT_CONFIG_FILE);
    assertNotNull(result);
  }

  /**
   * Run the ProxyServer(String,String,String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testProxyServer_2() throws Exception {
    // Modify the configuration to initialise an EVM client.
    JSONObject config = IOUtils.readJSONObjectFromFile(TestParameters.OUTPUT_CLIENT_CONFIG_FILE);
    config.put(ClientConfig.PURPOSE, ClientType.EVM);
    IOUtils.writeJSONToFile(config, TestParameters.OUTPUT_CLIENT_CONFIG_FILE);

    ProxyServer result = new ProxyServer(TestParameters.OUTPUT_CLIENT_CONFIG_FILE, TestParameters.DISTRICT_CONFIG_FILE);
    assertNotNull(result);
  }

  /**
   * Run the ProxyServer(String,String,String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testProxyServer_3() throws Exception {
    // Modify the configuration to initialise a VPS client.
    JSONObject config = IOUtils.readJSONObjectFromFile(TestParameters.OUTPUT_CLIENT_CONFIG_FILE);
    config.put(ClientConfig.PURPOSE, ClientType.VPS);
    IOUtils.writeJSONToFile(config, TestParameters.OUTPUT_CLIENT_CONFIG_FILE);

    ProxyServer result = new ProxyServer(TestParameters.OUTPUT_CLIENT_CONFIG_FILE, TestParameters.DISTRICT_CONFIG_FILE);
    assertNotNull(result);
  }

  /**
   * Run the void startServer(int,File) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testStartServer_1() throws Exception {
    ProxyServer fixture = new ProxyServer(TestParameters.OUTPUT_CLIENT_CONFIG_FILE, TestParameters.DISTRICT_CONFIG_FILE);

    fixture.startServer(TestParameters.PROXY_PORTS[1], new File(TestParameters.PROXY_ROOT));

    // Attempt to connect to the server.
    Socket socket = null;

    try {
      socket = new Socket("localhost", TestParameters.PROXY_PORTS[1]);
      assertNotNull(socket.getInetAddress());
    }
    // Pass all exceptions up.
    finally {
      socket.close();
    }

    fixture.stopServer();
  }

  /**
   * Run the void startServer(int,File) method test.
   * 
   * @throws Exception
   */
  @Test(expected = uk.ac.surrey.cs.tvs.ballotgen.exceptions.ProxyException.class)
  public void testStartServer_2() throws Exception {
    ProxyServer fixture = new ProxyServer(TestParameters.OUTPUT_CLIENT_CONFIG_FILE, TestParameters.DISTRICT_CONFIG_FILE);

    fixture.startServer(TestParameters.PROXY_PORTS[2], new File(TestParameters.PROXY_ROOT));
    fixture.startServer(TestParameters.PROXY_PORTS[2], new File(TestParameters.PROXY_ROOT));
    fixture.stopServer();
  }

  /**
   * Run the void stopServer() method test.
   * 
   * @throws Exception
   */
  @Test(expected = ConnectException.class)
  public void testStopServer_1() throws Exception {
    ProxyServer fixture = new ProxyServer(TestParameters.OUTPUT_CLIENT_CONFIG_FILE, TestParameters.DISTRICT_CONFIG_FILE);

    fixture.startServer(TestParameters.PROXY_PORTS[3], new File(TestParameters.PROXY_ROOT));

    // Attempt to connect to the server.
    Socket socket = null;

    try {
      socket = new Socket("localhost", TestParameters.PROXY_PORTS[3]);
      assertNotNull(socket.getInetAddress());
    }
    // Pass all exceptions up.
    finally {
      socket.close();
    }

    fixture.stopServer();

    // Attempt to connect to the server.
    try {
      socket = new Socket("localhost", TestParameters.PROXY_PORTS[3]);
      assertNull(socket.getInetAddress());
    }
    // Pass all exceptions up.
    finally {
      socket.close();
    }
  }

  /**
   * Run the void stopServer() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testStopServer_2() throws Exception {
    ProxyServer fixture = new ProxyServer(TestParameters.OUTPUT_CLIENT_CONFIG_FILE, TestParameters.DISTRICT_CONFIG_FILE);

    fixture.stopServer();
  }
}