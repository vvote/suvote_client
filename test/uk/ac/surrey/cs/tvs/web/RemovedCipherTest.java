/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * The class <code>RemovedCipherTest</code> contains tests for the class <code>{@link RemovedCipher}</code>.
 */
public class RemovedCipherTest {

  /**
   * Run the int compareTo(RemovedCipher) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCompareTo_1() throws Exception {
    RemovedCipher fixture = new RemovedCipher(10, 11);

    assertTrue(fixture.compareTo(new RemovedCipher(10, 11)) == 0);
    assertTrue(fixture.compareTo(new RemovedCipher(9, 11)) > 0);
    assertTrue(fixture.compareTo(new RemovedCipher(11, 11)) < 0);
  }

  /**
   * Run the RemovedCipher(int,int) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testRemovedCipher_1() throws Exception {
    RemovedCipher result = new RemovedCipher(10, 11);
    assertNotNull(result);
    assertEquals(10, result.getIndex());
    assertEquals(11, result.getCandidateIndex());
  }
}